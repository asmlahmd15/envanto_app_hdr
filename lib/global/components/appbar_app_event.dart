import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/modules/home/view/widget/icon_home.dart';
import 'package:flutter/material.dart';

AppBar appbarAppEvent({required String title,  IconData icon=Icons.list})=>AppBar(
  title:   TextAppTitle(text: title),
  backgroundColor: Colors.transparent,
  elevation: 0.0,
  actions:   [
    IconHome(
      icon: icon,
    ),
  ],
);