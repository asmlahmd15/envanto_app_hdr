import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class ButtonApp extends StatelessWidget {
  final String text;
  final void Function()? onTap;
  final double circular;

  const ButtonApp({Key? key, required this.text, this.onTap,   this.circular=10}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: onTap,
        child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 14),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(circular),
              color: ColorApp.fuchsia(context),
            ),
            child: Center(
                child: Text(
              text,
              style: Theme.of(context).textTheme.titleMedium,
            ))),
      ),
    );
  }
}
