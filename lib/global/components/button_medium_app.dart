import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class ButtonMediumApp extends StatelessWidget {
  final String text;
  final void Function()? onTap;
  final double? width;
  final double? height;
  final double ? fontSize;

  const ButtonMediumApp(
      {Key? key, required this.text, this.onTap, this.width, this.height, this.fontSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
          width: width ?? 130,
          height: height ?? 50,
          // padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: ColorApp.fuchsia(context),
          ),
          child: Center(
            child: Text(
              text,
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(fontWeight: FontWeight.bold, fontSize: fontSize??20),
            ),
          )),
    );
  }
}
