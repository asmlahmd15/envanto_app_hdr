import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class ButtonSmailApp extends StatelessWidget {
  final String text;

  final double? width;
  final double? height;
  final double? size ;

  final void Function()? onTap;

  const ButtonSmailApp(
      {Key? key, required this.text, this.onTap, this.width, this.height, this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: onTap,
        child: Container(
            width: width ?? MediaQuery.of(context).size.width / 2.2,
            height: height ?? 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            // padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 14),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: ColorApp.fuchsia(context),
            ),
            child: Center(
                child: Text(
              text,
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontSize: size?? 18,
              ),
            ))),
      ),
    );
  }
}
