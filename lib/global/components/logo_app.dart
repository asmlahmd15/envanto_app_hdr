import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

class LogoApp extends StatelessWidget {
  const LogoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(0, 37, 0, 0),
        child: Image.asset(
          ImageAssets.logoApp,
          fit: BoxFit.cover,
          width: 250,
          height: 50,
        ));
  }
}
