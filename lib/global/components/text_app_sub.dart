import 'package:flutter/material.dart';

class TextAppSub extends StatelessWidget {
 final String text;
  const TextAppSub({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: const AlignmentDirectional(0, 0),
      child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(47, 0, 47, 37),
          child: SizedBox(
            width: MediaQuery.of(context).size.width*0.6,
            child: Text(
              text,
              textAlign: TextAlign.center,

            ),
          ) ),
    );
  }
}
