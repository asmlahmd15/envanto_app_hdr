import 'package:flutter/material.dart';

class TextDisplayLarge extends StatelessWidget {
  final String text;
  final double ? sizeFont;
  const TextDisplayLarge({Key? key, required this.text,   this.sizeFont}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Text(text,style: Theme.of(context).textTheme.displayLarge!.copyWith(
      fontSize: sizeFont ?? 24,
    ));
  }
}
