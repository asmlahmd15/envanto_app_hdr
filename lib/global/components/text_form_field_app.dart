import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class TextFormFieldApp extends StatelessWidget {
  final TextEditingController? controller;
  final String hintText;
  final String labelText;
  final String? Function(String?)? validator;
  final bool? obscureText;
  final TextInputType keyboardType;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final void Function(String?)? onChanged;
  final void Function()? onTap;
  final void Function(String?)? onFieldSubmitted;
  final TextInputAction? textInputAction;
  final FocusNode? focusNode;
  final double vertical;

  const TextFormFieldApp(
      {Key? key,
      this.controller,
      required this.hintText,
      required this.labelText,
      this.validator,
      this.obscureText,
      this.keyboardType=TextInputType.text,
      this.suffixIcon,
      this.prefixIcon,
      this.onChanged,
      this.onTap,
      this.onFieldSubmitted,
      this.textInputAction,
      this.focusNode,
        this.vertical=20.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
            EdgeInsetsDirectional.symmetric(horizontal: 10,vertical: vertical),
      child: TextFormField(


        keyboardType: keyboardType,
        // controller: _model.firstNameController,
        obscureText: obscureText?? false,
        decoration: InputDecoration(
          suffixIcon: suffixIcon,
          labelText: labelText,
          labelStyle:
              Theme.of(context).textTheme.bodyMedium!.copyWith(fontSize: 16),
          hintText: hintText,
          hintStyle: Theme.of(context).textTheme.bodyMedium,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ColorApp.white(context).withOpacity(0.4),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(50),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ColorApp.white(context).withOpacity(0.5),
              width: 2,
            ),
            borderRadius: BorderRadius.circular(50),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ColorApp.blackSecond(context),
              width: 2,
            ),
            borderRadius: BorderRadius.circular(50),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ColorApp.blackSecond(context),
              width: 2,
            ),
            borderRadius: BorderRadius.circular(50),
          ),
          filled: true,
          // fillColor: FlutterFlowTheme.of(context).secondaryBackground,
          contentPadding:
              const EdgeInsetsDirectional.fromSTEB(16, 24, 24, 24),
        ),
        style: Theme.of(context).textTheme.bodySmall!.copyWith(
              fontSize: 16,
            ),
        // validator: _model.firstNameControllerValidator.asValidator(context),
      ),
    );
  }
}
