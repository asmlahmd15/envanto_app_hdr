import 'package:flutter/material.dart';

class TitleBody extends StatelessWidget {
  final String text;
  const TitleBody({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
        text,
        style: Theme.of(context).textTheme.displayMedium);
  }
}
