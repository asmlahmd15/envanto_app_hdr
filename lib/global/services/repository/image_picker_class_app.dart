
import 'package:image_picker/image_picker.dart';

class ImagePickerClassApp {
  Future<void> getLostData() async {
    final ImagePicker picker = ImagePicker();
    final LostDataResponse response = await picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    final List<XFile>? files = response.files;
    if (files != null) {
      _handleLostFiles(files);
    } else {
      _handleError(response.exception);
    }
  }

  void _handleLostFiles(List<XFile> files) {

    for (final file in files) {
      print('Lost file path: ${file.path}');

    }
  }

  void _handleError(Exception? exception) {

    if (exception != null) {
      print('Error retrieving lost data: $exception');

    }
  }
}

