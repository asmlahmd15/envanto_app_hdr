import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData darkThemes = ThemeData.dark().copyWith(
    brightness: Brightness.dark,
    primaryColor: const Color(0xFFED157A),
    indicatorColor: const Color(0xFF22282F),
    dividerColor: const Color(0xFF95A1AC),
    canvasColor: const Color(0xFF161630),
    hoverColor: const Color(0xFF1D2428),
    focusColor: const Color(0xFF262D34),
    colorScheme: const ColorScheme.dark(
      onPrimary: Color(0xFF57636C),
      onSecondary: Color(0xFF95A1AC),
      onSurface: Color(0xFF4B39EF),
      primary: Color(0xFF14181B),
      outline: Color(0xFF0C0C20),
      secondary: Color(0xFFFFFFFF),
      surface: Colors.grey,
      background: Color(0xFF39D2C0),
      // onBackground: ,
    ),
    textTheme: TextTheme(
      titleLarge: TextStyle(),
      titleMedium: GoogleFonts.readexPro(
        color: Color(0xFFFFFFFF),
        fontWeight: FontWeight.normal,
        fontSize: 18.0,
      ),
      titleSmall: GoogleFonts.readexPro(
        color: Color(0xFFED157A),
        fontWeight: FontWeight.w500,
        fontSize: 16.0,
      ),
      bodyLarge: TextStyle(),
      bodyMedium: GoogleFonts.urbanist(
        color: Color(0xFFFFFFFF),
        letterSpacing: 1.0,
        fontWeight: FontWeight.w500,
      ),
      bodySmall: GoogleFonts.readexPro(
        color: Color(0xFFFFFFFF),
        fontSize: 12.0,
        fontWeight: FontWeight.normal,
      ),
      displayMedium: GoogleFonts.readexPro(
        color: Color(0xFF0C0C20),
        fontSize: 12,
        fontWeight: FontWeight.normal,
      ),
      displayLarge: GoogleFonts.urbanist(
        color: Color(0xFFFFFFFF),
        fontWeight: FontWeight.w600,
        fontSize: 24.0,
      ),
      displaySmall: GoogleFonts.lexendDeca(
        color: Color(0xFFFFFFFF).withOpacity(0.5),
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
    ));
