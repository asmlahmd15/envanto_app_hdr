import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData lightThemes = ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColor: const Color(0xFFED157A),
    indicatorColor: Color(0xFFEEEEEE),
    dividerColor: const Color(0xFF95A1AC),
    canvasColor: const Color(0xFF161630),


    hoverColor: const Color(0xFFF5F5F5),



    focusColor: const Color(0xFF262D34),
    colorScheme: const ColorScheme.light(
      onPrimary: Color(0xFF57636C),
      onSecondary: Color(0xFF95A1AC),
      onSurface: Color(0xFF4B39EF),
      primary: Color(0xFFFFFFFF),
      outline: Color(0xFF0C0C20),


      secondary: Color(0xFF0C0C20),



      surface: Colors.grey,
      background: Color(0xFF39D2C0),
      // onBackground: ,
    ),
    textTheme: TextTheme(
      titleLarge: TextStyle(),
      titleMedium: GoogleFonts.readexPro(
        color: Color(0xFF0C0C20),
        fontWeight: FontWeight.normal,
        fontSize: 18.0,
      ),
      titleSmall: GoogleFonts.readexPro(
        color: Color(0xFFED157A),
        fontWeight: FontWeight.w500,
        fontSize: 16.0,
      ),
      bodyLarge: TextStyle(),
      bodyMedium: GoogleFonts.urbanist(
        color: Color(0xFF0C0C20),
        letterSpacing: 1.0,
        fontWeight: FontWeight.w500,
      ),
      bodySmall: GoogleFonts.readexPro(
        color: Color(0xFF0C0C20),
        fontSize: 12.0,
        fontWeight: FontWeight.normal,
      ),
      displayMedium: GoogleFonts.readexPro(
        color: Color(0xFFFFFFFF),
        fontSize: 12,
        fontWeight: FontWeight.normal,
      ),
      displayLarge: GoogleFonts.urbanist(
        color: Color(0xFF0C0C20),
        fontWeight: FontWeight.w600,
        fontSize: 24.0,
      ),
      displaySmall: GoogleFonts.lexendDeca(
        color: Color(0xFF0C0C20).withOpacity(0.5),
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
    ));
