import 'package:flutter/material.dart';

class ColorApp {
  static Color white(BuildContext context) {
    return Theme.of(context).colorScheme.secondary;
  }

  static Color secondWhite(BuildContext context) {
    return Theme.of(context).colorScheme.surface;
  }

  static Color blackSecond(BuildContext context) {
    return Theme.of(context).colorScheme.outline;
  }

  static Color mauve(BuildContext context) {
    return Theme.of(context).colorScheme.onSurface;
  }

  static Color fuchsia(BuildContext context) {
    return Theme.of(context).primaryColor;
  }

  static Color black(BuildContext context) {
    return Theme.of(context).colorScheme.primary;
  }

  static Color revealSilver(BuildContext context) {
    return Theme.of(context).colorScheme.onSecondary;
  }

  static Color secondSilverDetector(BuildContext context) {
    return Theme.of(context).colorScheme.onPrimary;
  }

  static Color leadBlack(BuildContext context) {
    return Theme.of(context).focusColor;
  }

  static Color primaryBackground(BuildContext context) {
    return Theme.of(context).hoverColor;
  }

  static Color purpleDark(BuildContext context) {
    return Theme.of(context).canvasColor;
  }

  static Color ashen(BuildContext context) {
    return Theme.of(context).dividerColor;
  }

  static Color revealBlack(BuildContext context) {
    return Theme.of(context).indicatorColor;
  }

  static Color grey(BuildContext context) {
    return Theme.of(context).colorScheme.background;
  }

  static Color get secondary =>
      _getColor(LightColors.secondary, DarkColors.secondary);

  static Color _getColor(Color lightColor, Color darkColor) {
    final brightness = WidgetsBinding.instance!.window.platformBrightness;
    return brightness == Brightness.dark ? darkColor : lightColor;
  }
}

class DarkColors {
  // static Color white = const Color(0xFFFFFFFF);
  // static Color secondWhite = const Color(0xC8FFFFFF);
  static Color secondary = const Color(0xFF39D2C0);
  static Color fuchsia = const Color(0xFFED157A);
  static Color ashen = const Color(0xFF95A1AC);
  static Color grey = Colors.grey;
  static Color revealBlack = const Color(0xFF22282F);
  static Color mauve = const Color(0xFF4B39EF);
  static Color black = const Color(0xFF14181B);
  static Color blackSecond = const Color(0xFF0C0C20);
  static Color primaryBackground = const Color(0xFF1D2428);
  static Color revealSilver = const Color(0xFF95A1AC);
  static Color purpleDark = const Color(0xFF161630);
  static Color secondSilverDetector = const Color(0xFF57636C);
  static Color leadBlack = const Color(0xFF262D34);
}

class LightColors {
  // static Color white = const Color(0xFF14181B);
  // static Color secondWhite = const Color(0xFF0C0C20);
  static Color secondary = const Color(0xFF39D2C0);
  // static Color fuchsia = const Color(0xFFED157A);
  // static Color ashen = const Color(0xFF95A1AC);
  // static Color grey = Colors.grey;
  // static Color revealBlack = const Color(0xFFFFFFFF);
  // static Color mauve = const Color(0xFF4B39EF);
  // static Color black = const Color(0xFFFFFFFF);
  // static Color blackSecond = const Color(0xC8FFFFFF);
  // static Color primaryBackground = const Color(0xFF1D2428);
  // static Color revealSilver = const Color(0xFF95A1AC);
  // static Color purpleDark = const Color(0xFF161630);
  // static Color secondSilverDetector = const Color(0xFF57636C);
  // static Color leadBlack = const Color(0xFF262D34);
}
