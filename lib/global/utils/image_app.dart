class ImageAssets {
  static const String onboardImage = 'assets/images/test1.jpg';
  static const String logoApp = 'assets/images/evanto_logo.png';
  static const String step1 = 'assets/images/step_1.png';
  static const String step2 = 'assets/images/step_2.png';
  static const String step3 = 'assets/images/step_3.png';
  static const String step4 = 'assets/images/step_4.png';
  static const String step5 = 'assets/images/step_5.png';
  static const String step6 = 'assets/images/step_6.png';
  static const String step7 = 'assets/images/step_7.png';
  static const String step8 = 'assets/images/step_8.png';
  static const String step9 = 'assets/images/step_9.png';
  static const String rectangle = 'assets/images/Rectangle.png';
  static const String rectangle1 = 'assets/images/Rectangle-1.png';
  static const String rectangle2 = 'assets/images/Rectangle-2.png';

  static const String sports1 = 'assets/images/sports_1.png';
  static const String sports2 = 'assets/images/sports_2.png';
  static const String sports3 = 'assets/images/sports_3.png';
  static const String sports4 = 'assets/images/sports_4.png';
  static const String sports5 = 'assets/images/sports_5.png';
  static const String sports6 = 'assets/images/sports_6.png';
  static const String sports7 = 'assets/images/sports_7.png';
  static const String sports8 = 'assets/images/sports_8.png';
  static const String sports9 = 'assets/images/sports_9.png';



  static const String book = 'assets/images/book.png';
  static const String iconApp = 'assets/images/asset_1.png';
  static const String bookT = 'assets/images/book_t.png';
  static const String calendar = 'assets/images/calendar.png';
  static const String discoBall = 'assets/images/disco-ball.png';
  static const String event = 'assets/images/event.png';
  static const String gameController = 'assets/images/game-controller.png';
  static const String moon = 'assets/images/moon.png';
  static const String music = 'assets/images/music.png';
  static const String musicT = 'assets/images/musicT.png';
  static const String screen = 'assets/images/screen.jpg';


  static const String partyPhoto = 'assets/images/party_photo.jpg';
  static const String partyPerson = 'assets/images/party_person.jpg';





}












