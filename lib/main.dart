import 'package:envanto_app/global/themes/theme_notifier.dart';
import 'package:envanto_app/global/themes/themes.dart';
import 'package:envanto_app/modules/auth/view/screen/sign_and_login.dart';
import 'package:envanto_app/modules/book_now/view/screen/book_now_screen.dart';
import 'package:envanto_app/modules/booking_details/view/screen/booking_details_screen.dart';
import 'package:envanto_app/modules/change%20password/view/screen/change_password_screen.dart';
import 'package:envanto_app/modules/create_profile/view/screen/create_profile_screen.dart';
import 'package:envanto_app/modules/edite_profile/view/screen/edite_profile_screen.dart';
import 'package:envanto_app/modules/event_details/view/screen/event_details_screen.dart';
import 'package:envanto_app/modules/home/view/screen/home_screen.dart';
import 'package:envanto_app/modules/layout/view/screen/layout_screen.dart';
import 'package:envanto_app/modules/login/view/screen/login_screen.dart';
import 'package:envanto_app/modules/onboarding/view/screen/onboarding_screen.dart';
import 'package:envanto_app/modules/reels/view/screen/reels_screen.dart';
import 'package:envanto_app/modules/search/view/screen/map_app_screen.dart';
import 'package:envanto_app/modules/search/view/screen/search_screen.dart';
import 'package:envanto_app/modules/signup/view/screen/signup_screen.dart';
import 'package:envanto_app/modules/step/layers_step/view/screen/layers_step_screen.dart';
import 'package:envanto_app/modules/step/screen/step_one_screen.dart';
import 'package:envanto_app/modules/view_profile/view/screen/view_profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    ChangeNotifierProvider<ThemeNotifier>(
      create: (_) => ThemeNotifier(Themes.customLightTheme),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      darkTheme: Themes.customDarkTheme,
      theme: Themes.customLightTheme,
      title: 'Flutter Demo',
      themeMode: themeNotifier.getTheme() == Themes.customDarkTheme
          ? ThemeMode.light
          : ThemeMode.dark,
      // themeMode:   ThemeMode.dark ,
      // home: ReelAppScreen(),
      // home: LayersStepScreen(),
      home:   LayoutScreen(),
    );
  }
}
