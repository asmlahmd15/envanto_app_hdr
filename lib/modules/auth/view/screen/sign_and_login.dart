import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/components/logo_app.dart';
import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/auth/view/widget/signin_screen.dart';
import 'package:envanto_app/modules/auth/view/widget/signup_screen.dart';
import 'package:flutter/material.dart';

class SignAndLogin extends StatefulWidget {
  const SignAndLogin({Key? key}) : super(key: key);

  @override
  State<SignAndLogin> createState() => _SignAndLoginState();
}

class _SignAndLoginState extends State<SignAndLogin>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(ImageAssets.onboardImage),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Column(
              children: [
                const LogoApp(),
                TabBar(
                  indicatorColor: ColorApp.fuchsia(context),
                  controller: _tabController,
                  tabs: const [
                    Tab(
                      child: TextAppTitle(text: 'Sign In'),
                    ),
                    Tab(
                      child: TextAppTitle(text: 'Sign Up'),
                    ),
                  ],
                ),
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      SinInScreen(),

                      SinUpScreen(),

                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

}
