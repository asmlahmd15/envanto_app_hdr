import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

Widget buildCircleAvatarIcon(IconData iconData,BuildContext context) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: CircleAvatar(
      radius: 25,
      backgroundColor: ColorApp.revealBlack(context),
      child: Icon(
        iconData,
        color: ColorApp.white(context),
      ),
    ),
  );
}