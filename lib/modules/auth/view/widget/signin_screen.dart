import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/auth/view/widget/build_circle_avatarIcon.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/form_field_step_app.dart';
import 'package:flutter/material.dart';

class SinInScreen extends StatefulWidget {
  const SinInScreen({Key? key}) : super(key: key);

  @override
  State<SinInScreen> createState() => _SinInScreenState();
}

class _SinInScreenState extends State<SinInScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FromFieldStepApp(
          hintText: 'enter you mobile',
        ),
        FromFieldStepApp(
          hintText: 'Enter password',
          icon: Icon(Icons.visibility_outlined),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: ButtonApp(
            text: 'Log in',
            onTap: () {},
          ),
        ),
        TextAppTitle(text: 'Forgot Password?'),
        TextAppSub(text: 'Or use a social account to login'),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildCircleAvatarIcon(Icons.facebook,context),
            buildCircleAvatarIcon(Icons.call,context),
            buildCircleAvatarIcon(Icons.email,context),
          ],
        )
      ],
    );
  }
}


