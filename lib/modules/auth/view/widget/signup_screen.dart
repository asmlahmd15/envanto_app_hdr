import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/modules/auth/view/widget/build_circle_avatarIcon.dart';
import 'package:envanto_app/modules/layout/view/screen/layout_screen.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/form_field_step_app.dart';
import 'package:flutter/material.dart';

class SinUpScreen extends StatefulWidget {
  const SinUpScreen({Key? key}) : super(key: key);

  @override
  State<SinUpScreen> createState() => _SinUpScreenState();
}

class _SinUpScreenState extends State<SinUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const FromFieldStepApp(
          hintText: 'enter you mobile',
        ),
        const FromFieldStepApp(
          hintText: 'Enter password',
          icon: Icon(Icons.visibility_outlined),
        ),

        const FromFieldStepApp(
          hintText: 'Retype the password',
          icon: Icon(Icons.visibility_outlined),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.01),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: ButtonApp(
            text: 'Create Account',
            onTap: () {

              navigateAndFinish(context, LayoutScreen());

            },
          ),
        ),

        const TextAppSub(text: 'Or use a social account to create account'),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildCircleAvatarIcon(Icons.facebook,context),
            buildCircleAvatarIcon(Icons.call,context),
            buildCircleAvatarIcon(Icons.email,context),
          ],
        )
      ],
    );
  }
}
