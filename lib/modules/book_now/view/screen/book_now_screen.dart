import 'package:envanto_app/global/components/appbar_app_event.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/book_now/view/widget/count_widget.dart';
import 'package:envanto_app/modules/book_now/view/widget/credit_card_form.dart';
import 'package:envanto_app/modules/booking_details/view/widget/dates_event_bill.dart';
import 'package:envanto_app/modules/search/view/widget/item_previous_search.dart';
import 'package:flutter/material.dart';

class BookNowScreen extends StatefulWidget {
  const BookNowScreen({Key? key}) : super(key: key);

  @override
  State<BookNowScreen> createState() => _BookNowScreenState();
}

class _BookNowScreenState extends State<BookNowScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorApp.black(context),
      appBar: appbarAppEvent(title: 'Book Now'),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),

                  Container(
                    height: 120,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      image: const DecorationImage(
                        image: AssetImage(ImageAssets.partyPerson),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text('Event Details',
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium!
                            .copyWith(fontSize: 24)),
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: Text('marshmallow',
                        style: Theme.of(context)
                            .textTheme
                            .bodySmall!
                            .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
                  ),

                  // Divider(color: ColorApp.leadBlack,),
                  Divider(
                    color: ColorApp.secondSilverDetector(context),
                  ),

                  Text('Number of Guests',
                      style: Theme.of(context)
                          .textTheme
                          .displaySmall!
                          .copyWith(color: ColorApp.white(context).withOpacity(0.6))),

                  const CountWidget(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text('Choose an Option',
                        style: Theme.of(context)
                            .textTheme
                            .displaySmall!
                            .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
                  ),

                  const Wrap(
                    spacing: 8,
                    runSpacing: 4,
                    children: [
                      ItemPreviousSearch(title: 'Drink'),
                      ItemPreviousSearch(title: 'Dinner'),
                      ItemPreviousSearch(title: 'Tour'),
                      ItemPreviousSearch(title: 'Transportation'),
                    ],
                  ),

                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    child: Text('Payment Information',
                        style: Theme.of(context)
                            .textTheme
                            .displaySmall!
                            .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
                  ),
                  // const Spacer(),
                  const CreditCardForm(),

                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        DatesEventBill(
                          title: 'Base Price',
                          titleT: '156.000 Sp',
                        ),
                        DatesEventBill(
                          title: 'Taxes',
                          titleT: '24.000 sp',
                        ),
                        DatesEventBill(
                          title: 'Cleaning Fee',
                          titleT: '30.000 sp',
                        ),
                        DatesEventBill(
                          title: 'Total',
                          titleT: '219.000 sp',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              highlightColor: Colors.transparent,
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              onTap: () {},
              child: Container(
                height: 80,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: ColorApp.fuchsia(context),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Center(
                      child: Text(
                    'Book Now',
                    style: Theme.of(context).textTheme.displaySmall!.copyWith(
                          color: ColorApp.white(context),
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                        ),
                  )),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
