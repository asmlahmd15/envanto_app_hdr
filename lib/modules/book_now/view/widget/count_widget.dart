import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class CountWidget extends StatefulWidget {
  const CountWidget({Key? key}) : super(key: key);

  @override
  State<CountWidget> createState() => _CountWidgetState();
}

class _CountWidgetState extends State<CountWidget> {
  int count = 1;
  @override
  Widget build(BuildContext context) {
    return
      Padding(
        padding: EdgeInsetsDirectional.fromSTEB(0, 12, 0, 0),
        child: Container(
          width: 160,
          height: 50,
          decoration: BoxDecoration(
            color: ColorApp.primaryBackground(context),
            borderRadius: BorderRadius.circular(40),
            shape: BoxShape.rectangle,
            border: Border.all(
              color: ColorApp.ashen(context).withOpacity(0.3),
              width: 1,
            ),
          ),

          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [

              InkWell(
                  onTap: (){
                    setState(() {
                      if(count > 1){
                        count--;
                      }
                    });
                  },
                  child: Icon(Icons.remove,color: ColorApp.ashen(context),size: 28)),

              Text('$count',style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                fontSize: 20,
              ),),
              InkWell(
                  onTap: (){
                    setState(() {
                      count++;
                    });
                  },
                  child: Icon(Icons.add,color: ColorApp.fuchsia(context),size: 28)),

            ],
          ),

        ),
      )
    ;
  }
}
