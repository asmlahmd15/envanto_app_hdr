import 'package:envanto_app/global/utils/color_app.dart';
 import 'package:flutter/material.dart';

class CreditCardForm extends StatelessWidget {
  const CreditCardForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(12, 0, 12, 0),
      child: FlutterFlowCreditCardForm(

        obscureNumber: true,
        obscureCvv: false,
        spacing: 10,
        textStyle: Theme.of(context).textTheme.titleSmall,
        inputDecoration: InputDecoration(

          filled: true,
          fillColor: ColorApp.black(context),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ColorApp.leadBlack(context),
              width: 2,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              // color: ColorApp.purpleDark,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      ),
    );
  }
}



class FlutterFlowCreditCardForm extends StatefulWidget {
  const FlutterFlowCreditCardForm({
    Key? key,
    this.obscureNumber = false,
    this.obscureCvv = false,
    this.textStyle,
    this.spacing = 10.0,
    this.inputDecoration = const InputDecoration(
      border: OutlineInputBorder(),
    ),
  }) : super(key: key);
  final bool obscureNumber;
  final bool obscureCvv;
  final TextStyle? textStyle;
  final double spacing;
  final InputDecoration inputDecoration;

  @override
  _FlutterFlowCreditCardFormState createState() =>
      _FlutterFlowCreditCardFormState();
}

class _FlutterFlowCreditCardFormState extends State<FlutterFlowCreditCardForm> {
  final MaskedTextController _cardNumberController =
  MaskedTextController(mask: '0000 0000 0000 0000');
  final TextEditingController _expiryDateController =
  MaskedTextController(mask: '00/00');
  final TextEditingController _cvvCodeController =
  MaskedTextController(mask: '0000');

  FocusNode cvvFocusNode = FocusNode();
  FocusNode cardNumberNode = FocusNode();
  FocusNode expiryDateNode = FocusNode();

  void textFieldFocusDidChange() {
    // widget.creditCardModel.isCvvFocused = cvvFocusNode.hasFocus;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    cvvFocusNode.dispose();
    expiryDateNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Form(

    child: Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(top: 12.0),
          child: TextFormField(
            obscureText: widget.obscureNumber,
            controller: _cardNumberController,
            onEditingComplete: () =>
                FocusScope.of(context).requestFocus(expiryDateNode),
            style: widget.textStyle,
            decoration: widget.inputDecoration.copyWith(
              labelText: 'Card number',
              hintText: 'XXXX XXXX XXXX XXXX',
              labelStyle: widget.textStyle,
              hintStyle: widget.textStyle,
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value == null || value.isEmpty || value.length < 16) {
                return 'Please input a valid number';
              }
              return null;
            },
          ),
        ),
        SizedBox(height: widget.spacing),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: TextFormField(
                  controller: _expiryDateController,
                  focusNode: expiryDateNode,
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(cvvFocusNode);
                  },
                  style: widget.textStyle,
                  decoration: widget.inputDecoration.copyWith(
                    labelText: 'Exp. Date',
                    hintText: 'MM/YY',
                    labelStyle: widget.textStyle,
                    hintStyle: widget.textStyle,
                  ),
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input a valid date';
                    }

                    final DateTime now = DateTime.now();
                    final List<String> date = value.split(RegExp(r'/'));
                    final int month = int.parse(date.first);
                    final int year = int.parse('20${date.last}');
                    final DateTime cardDate = DateTime(year, month);

                    if (cardDate.isBefore(now) ||
                        month > 12 ||
                        month == 0) {
                      return 'Please input a valid date';
                    }
                    return null;
                  },
                ),
              ),
            ),
            const SizedBox(width: 16.0),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: TextFormField(
                  obscureText: widget.obscureCvv,
                  focusNode: cvvFocusNode,
                  controller: _cvvCodeController,
                  style: widget.textStyle,
                  decoration: widget.inputDecoration.copyWith(
                    labelText: 'CVV',
                    hintText: 'XXX',
                    labelStyle: widget.textStyle,
                    hintStyle: widget.textStyle,
                  ),
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  validator: (value) {
                    if (value == null ||
                        value.isEmpty ||
                        value.length < 3) {
                      return 'Please input a valid CVV';
                    }
                    return null;
                  },
                ),
              ),
            ),
          ],
        ),
      ],
    ),
  );


  Map<CardType, Set<List<String>>> cardNumPatterns =
  <CardType, Set<List<String>>>{
    CardType.visa: <List<String>>{
      <String>['4'],
    },
    CardType.americanExpress: <List<String>>{
      <String>['34'],
      <String>['37'],
    },
    CardType.discover: <List<String>>{
      <String>['6011'],
      <String>['622126', '622925'],
      <String>['644', '649'],
      <String>['65']
    },
    CardType.mastercard: <List<String>>{
      <String>['51', '55'],
      <String>['2221', '2229'],
      <String>['223', '229'],
      <String>['23', '26'],
      <String>['270', '271'],
      <String>['2720'],
    },
  };


  CardType detectCCType(String cardNumber) {
    //Default card type is other
    CardType cardType = CardType.otherBrand;

    if (cardNumber.isEmpty) {
      return cardType;
    }

    cardNumPatterns.forEach(
          (type, patterns) {
        for (final patternRange in patterns) {
          // Remove any spaces
          String ccPatternStr = cardNumber.replaceAll(RegExp(r's+|s'), '');
          final int rangeLen = patternRange[0].length;
          // Trim the Credit Card number string to match the pattern prefix length
          if (rangeLen < cardNumber.length) {
            ccPatternStr = ccPatternStr.substring(0, rangeLen);
          }

          if (patternRange.length > 1) {
            // Convert the prefix range into numbers then make sure the
            // Credit Card num is in the pattern range.
            // Because Strings don't have '>=' type operators
            final int ccPrefixAsInt = int.parse(ccPatternStr);
            final int startPatternPrefixAsInt = int.parse(patternRange[0]);
            final int endPatternPrefixAsInt = int.parse(patternRange[1]);
            if (ccPrefixAsInt >= startPatternPrefixAsInt &&
                ccPrefixAsInt <= endPatternPrefixAsInt) {
              // Found a match
              cardType = type;
              break;
            }
          } else {
            // Just compare the single pattern prefix with the Credit Card prefix
            if (ccPatternStr == patternRange[0]) {
              // Found a match
              cardType = type;
              break;
            }
          }
        }
      },
    );

    return cardType;
  }
}


enum CardType {
  otherBrand,
  mastercard,
  visa,
  americanExpress,
  unionpay,
  discover,
  elo,
  hipercard,
}


class MaskedTextController extends TextEditingController {
  MaskedTextController(
      {String? text, required this.mask, Map<String, RegExp>? translator})
      : super(text: text) {
    this.translator = translator ?? MaskedTextController.getDefaultTranslator();

    addListener(() {
      final String previous = _lastUpdatedText;
      if (beforeChange(previous, this.text)) {
        updateText(this.text);
        afterChange(previous, this.text);
      } else {
        updateText(_lastUpdatedText);
      }
    });

    updateText(this.text);
  }

  String mask;

  late Map<String, RegExp> translator;

  Function afterChange = (String previous, String next) {};
  Function beforeChange = (String previous, String next) {
    return true;
  };

  String _lastUpdatedText = '';

  void updateText(String text) {
    if (text.isNotEmpty) {
      this.text = _applyMask(mask, text);
    } else {
      this.text = '';
    }

    _lastUpdatedText = this.text;
  }

  void updateMask(String mask, {bool moveCursorToEnd = true}) {
    this.mask = mask;
    updateText(text);

    if (moveCursorToEnd) {
      this.moveCursorToEnd();
    }
  }

  void moveCursorToEnd() {
    final String text = _lastUpdatedText;
    selection = TextSelection.fromPosition(TextPosition(offset: text.length));
  }

  @override
  set text(String newText) {
    if (super.text != newText) {
      super.text = newText;
      moveCursorToEnd();
    }
  }

  static Map<String, RegExp> getDefaultTranslator() {
    return <String, RegExp>{
      'A': RegExp(r'[A-Za-z]'),
      '0': RegExp(r'[0-9]'),
      '@': RegExp(r'[A-Za-z0-9]'),
      '*': RegExp(r'.*')
    };
  }

  String _applyMask(String? mask, String value) {
    String result = '';

    int maskCharIndex = 0;
    int valueCharIndex = 0;

    while (true) {
      // if mask is ended, break.
      if (maskCharIndex == mask!.length) {
        break;
      }

      // if value is ended, break.
      if (valueCharIndex == value.length) {
        break;
      }

      final String maskChar = mask[maskCharIndex];
      final String valueChar = value[valueCharIndex];

      // value equals mask, just set
      if (maskChar == valueChar) {
        result += maskChar;
        valueCharIndex += 1;
        maskCharIndex += 1;
        continue;
      }

      // apply translator if match
      if (translator.containsKey(maskChar)) {
        if (translator[maskChar]!.hasMatch(valueChar)) {
          result += valueChar;
          maskCharIndex += 1;
        }

        valueCharIndex += 1;
        continue;
      }

      // not masked value, fixed char on mask
      result += maskChar;
      maskCharIndex += 1;
      continue;
    }

    return result;
  }
}
