import 'package:envanto_app/global/components/appbar_app_event.dart';
import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/components/button_smail_app.dart';
import 'package:envanto_app/global/components/text_display_large.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/booking_details/view/widget/dates_event_bill.dart';
import 'package:flutter/material.dart';

class BookingDetailsScreen extends StatefulWidget {
  const BookingDetailsScreen({Key? key}) : super(key: key);

  @override
  State<BookingDetailsScreen> createState() => _BookingDetailsScreenState();
}

class _BookingDetailsScreenState extends State<BookingDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarAppEvent(title: 'Event Details',icon: Icons.more_vert_outlined),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Dates of event',
                  style: Theme.of(context).textTheme.bodyMedium),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: TextDisplayLarge(
                  text: 'Event Name',
                ),

              ),

              SizedBox(height: 10,),
              Container(
                height: 200,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: ColorApp.ashen(context)),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: TextDisplayLarge(
                  text: 'MMMEd',
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('event addrese',
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
                  const Icon(Icons.arrow_forward_ios),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Text('price breakdwon',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
              const SizedBox(
                height: 10,
              ),
              const DatesEventBill(
                title: 'Base Price',
                titleT: '156.000 Sp',
              ),
              const DatesEventBill(
                title: 'Taxes',
                titleT: '24.000 Sp',
              ),
              const DatesEventBill(
                title: 'Cleaning Fee',
                titleT: '30.000Sp',
              ),
              const SizedBox(
                height: 20,
              ),
              const DatesEventBill(
                title: 'Total',
                titleT: '219.000 Sp',
              ),
              const SizedBox(
                height: 20,
              ),
              Text('Your booking has been completed!',
                  style: Theme.of(context)
                      .textTheme
                      .displayLarge!
                      .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
              const Center(
                child: ButtonSmailApp(
                  text: 'Review Event',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
