import 'package:envanto_app/global/components/text_display_large.dart';
import 'package:flutter/material.dart';

class DatesEventBill extends StatelessWidget {
  final String title;
  final String titleT;
  const DatesEventBill({Key? key, required this.title, required this.titleT}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title,style: Theme.of(context).textTheme.bodyMedium),
          Padding(
          padding: EdgeInsets.symmetric(vertical: 2),
          child: TextDisplayLarge(
            text: titleT,
          ),
        ),



      ],);
  }
}
