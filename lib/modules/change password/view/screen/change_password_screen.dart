import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/form_field_step_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';





class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Column(
      children: [
        SizedBox(height: MediaQuery.of(context).size.height*0.1),

        SizedBox(height: MediaQuery.of(context).size.height*0.02),
        const TextAppTitle(text: 'Set your password'),

        SizedBox(height: MediaQuery.of(context).size.height*0.02),

        const TextAppSub(text: 'Enter a strong password for your account'),
        SizedBox(height: 20,),
        const ImageStep(pathUrl: ImageAssets.step3,),
        SizedBox(height: MediaQuery.of(context).size.height*0.02),

        SizedBox(height: 20,),
        const FromFieldStepApp(hintText: 'Enter the new password',
          icon: Icon(Icons.visibility_outlined),),


        Spacer(),


        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
          child: ButtonApp(
            onTap: (){},
            text: 'Continue',
          ),
        ),





      ],
    ),);
  }
}
