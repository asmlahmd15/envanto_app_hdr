import 'dart:io';

import 'package:envanto_app/global/components/appbar_app_event.dart';
import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/components/button_smail_app.dart';
import 'package:envanto_app/global/components/text_app_smail.dart';
import 'package:envanto_app/global/components/text_form_field_app.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CreateProfileScreen extends StatefulWidget {
  const CreateProfileScreen({Key? key}) : super(key: key);

  @override
  State<CreateProfileScreen> createState() => _CreateProfileScreenState();
}

class _CreateProfileScreenState extends State<CreateProfileScreen> {
  final ImagePicker _imagePicker = ImagePicker();
  String? selectedImagePath;

  void _selectImage() async {
    final XFile? pickedImage =
        await _imagePicker.pickImage(source: ImageSource.gallery);

    if (pickedImage != null) {
      setState(() {
        selectedImagePath = pickedImage.path;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarAppEvent(title: 'Create Profile'),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              selectedImagePath == null
                  ? Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        image: const DecorationImage(
                          image: AssetImage(ImageAssets.partyPhoto),
                          fit: BoxFit.cover,
                        ),
                      ),
                    )
                  : Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        image: DecorationImage(
                          image: FileImage(File(selectedImagePath!)),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
              const SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: _selectImage,
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: ColorApp.leadBlack(context),
                  ),
                  child: const TextAppSmail(text: 'Choose Photo'),
                ),
              ),
              const TextFormFieldApp(
                hintText: 'write your first name here..',
                labelText: 'First name',
              ),
              const TextFormFieldApp(
                hintText: 'write your last name here..',
                labelText: 'Last name',
              ),
              const TextFormFieldApp(
                hintText: 'Enter your username here... ',
                labelText: 'Username',
              ),
              const TextFormFieldApp(
                keyboardType: TextInputType.datetime,
                hintText: 'Enter your date of birth  here... ',
                labelText: 'Date of birth',
              ),
              ButtonApp(
                onTap: () {},
                circular: 30.0,
                text: 'Comfirm',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
