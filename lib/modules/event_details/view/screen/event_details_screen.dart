
import 'package:envanto_app/global/components/button_medium_app.dart';

import 'package:envanto_app/global/components/text_display_large.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/book_now/view/screen/book_now_screen.dart';
import 'package:envanto_app/modules/event_details/view/widget/icon_event_detsils.dart';
import 'package:envanto_app/modules/event_details/view/widget/page_view_event_details.dart';
import 'package:flutter/material.dart';


class EventDetailsScreen extends StatefulWidget {
  const EventDetailsScreen({Key? key}) : super(key: key);

  @override
  State<EventDetailsScreen> createState() => _EventDetailsScreenState();
}

class _EventDetailsScreenState extends State<EventDetailsScreen> {

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      bottomNavigationBar: Container(
        height: 100,
        decoration: BoxDecoration(
          color: ColorApp.white(context),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(25),
            topRight: Radius.circular(25),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              width: 20,
            ),
            Text(
              '156 sp',
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: ColorApp.black(context),
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              '+ taxes/fees',
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: ColorApp.grey(context),
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ButtonMediumApp(
                fontSize: 12,
                width: 130,
                height: 50,
                onTap: () {
                  navigateTo(context, const BookNowScreen());
                },
                text: 'Book Now',
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 50,
                ),
                Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.4,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      image: const DecorationImage(
                          image: AssetImage(ImageAssets.partyPerson),
                          fit: BoxFit.fill),
                    )),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: TextDisplayLarge(
                    text: 'Event Name',
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Text('marshmallow',
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
                ),
                Row(
                  children: [
                    const Icon(
                      Icons.star_rounded,
                      color: Color(0xFFFFA130),
                      size: 24,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Text(' ReviewsRecordList.toList Reviews',
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall!
                              .copyWith(
                                  color: ColorApp.white(context).withOpacity(0.6))),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Text('description',
                      style: Theme.of(context).textTheme.bodySmall),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Text(
                      'Marshmallow Event delivered a sweet and delightful experience, immersing attendees in a sugary wonderland of fun and flavors. From fluffy treats to captivating activities, it was a confectionery dream come true!',
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(color: ColorApp.white(context).withOpacity(0.6))),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Text('Amenities',
                      style: Theme.of(context).textTheme.bodySmall),
                ),
                const Row(
                  children: [
                    IconEventDetails(icon: Icons.pool_rounded),
                    IconEventDetails(icon: Icons.pets_rounded),
                    IconEventDetails(
                        icon: Icons.local_laundry_service_outlined),
                    IconEventDetails(icon: Icons.nightlife),
                    IconEventDetails(icon: Icons.theater_comedy),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Text('What people are saying',
                      style: Theme.of(context).textTheme.bodySmall),
                ),
                SizedBox(
                    height: MediaQuery.of(context).orientation == Orientation.portrait
                        ? screenHeight * 0.39
                        : screenHeight * 0.61,

                    child: const PageViewEventDetails()),
              ]),
        ),
      ),
    );
  }
}
