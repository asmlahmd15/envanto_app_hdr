import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';




class IconEventDetails extends StatefulWidget {
  final IconData icon;
  const IconEventDetails({Key? key, required this.icon}) : super(key: key);

  @override
  State<IconEventDetails> createState() => _IconEventDetailsState();
}

class _IconEventDetailsState extends State<IconEventDetails> {
  bool _isEnabled = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          _isEnabled = !_isEnabled;
        });
      },
      child: Padding(
        padding: const EdgeInsets.only(right: 8),
        child: Align(
          alignment: const AlignmentDirectional(0.0, 0.0),
          child: Container(
            width: 44.0,
            height: 44.0,
            decoration: BoxDecoration(
              color:  _isEnabled ? ColorApp.white(context) : ColorApp.primaryBackground(context),
              shape: BoxShape.circle,
              border: Border.all(
                color: ColorApp.ashen(context).withOpacity(0.5),
                width: 2.0,
              ),
            ),
            alignment: const AlignmentDirectional(0.0, 0.0),
            child: Icon(
              widget.icon,
              color: ColorApp.ashen(context),
            ),
          ),
        ),
      ),
    );
  }
}



