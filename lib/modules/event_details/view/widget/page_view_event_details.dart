import 'package:envanto_app/global/components/text_display_large.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class PageViewEventDetails extends StatefulWidget {
  const PageViewEventDetails({Key? key}) : super(key: key);

  @override
  State<PageViewEventDetails> createState() => _PageViewEventDetailsState();
}

class _PageViewEventDetailsState extends State<PageViewEventDetails> {
  final controller = PageController(viewportFraction: 0.8, keepPage: true);

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    final pages = List.generate(
        6,
        (index) => Container(


            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: ColorApp.primaryBackground(context),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, right: 14, left: 14),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextDisplayLarge(
                            sizeFont: 20,
                            text: 'DisplayName',
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.star_rounded,
                                color: Color(0xFFFFA130),
                                size: 24,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: Color(0xFFFFA130),
                                size: 24,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: Color(0xFFFFA130),
                                size: 24,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: Color(0xFFFFA130),
                                size: 24,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: Color(0xFFFFA130),
                                size: 24,
                              ),
                            ],
                          )
                        ],
                      ),
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          image: const DecorationImage(
                            image: AssetImage(ImageAssets.onboardImage),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Text(
                      'Exceeded all expectations, crafting a whimsical wonderland filled with fluffy delights and enchanting activities that left attendees with smiles as sweet as the treats themselves.',
                      style: Theme.of(context).textTheme.bodySmall!.copyWith(
                          color: ColorApp.white(context).withOpacity(0.6),
                          fontSize: 14)),
                ),
              ],
            )));
    return SizedBox(
      height: MediaQuery.of(context).size.height / 3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const SizedBox(height: 12),
          Expanded(
            child: PageView.builder(
              controller: controller,
              // itemCount: pages.length,
              itemBuilder: (_, index) {
                return pages[index % pages.length];
              },
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 10),
          ),
          SmoothPageIndicator(
            controller: controller,
            count: pages.length,
            effect: const WormEffect(
              dotHeight: 10,
              dotWidth: 10,
              type: WormType.thinUnderground,
            ),
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
