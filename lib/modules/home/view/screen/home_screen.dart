import 'package:envanto_app/global/components/appbar_app_event.dart';
import 'package:envanto_app/global/components/text_app_smail.dart';

import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/home/view/sub_event/culture_screen.dart';
import 'package:envanto_app/modules/home/view/sub_event/music_screen.dart';
import 'package:envanto_app/modules/home/view/sub_event/party_screen.dart';
import 'package:envanto_app/modules/home/view/sub_event/tonight_screen.dart';
import 'package:envanto_app/modules/home/view/sub_event/week_screen.dart';

import 'package:envanto_app/modules/home/view/widget/film_item_home.dart';
import 'package:envanto_app/modules/home/view/widget/home_item.dart';
import 'package:envanto_app/modules/layout/view/widget/popular_home_item.dart';

import 'package:envanto_app/modules/step/widget/image_step_url.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int? selectedHomeItemIndex;
  List<String> images = [];
  List<String> titles = [];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: ColorApp.black(context),
      appBar: appbarAppEvent(title: 'Home Page'),
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).orientation == Orientation.portrait
                ? height * 0.15
                : height * 0.3,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: imageHomeUrl.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      if (selectedHomeItemIndex == index) {
                        selectedHomeItemIndex = null;
                      } else {
                        selectedHomeItemIndex = index;
                      }
                    });
                  },
                  child: HomeItem(
                    image: imageHomeUrl[index],
                    title: imageNameHome[index],
                    isSelected: selectedHomeItemIndex == index,
                  ),
                );
              },
            ),
          ),
          Expanded(
            child: _displayContentBasedOnSelection(),
          ),
        ],
      ),
    );
  }

  Widget _displayContentBasedOnSelection() {
    if (selectedHomeItemIndex == null) {
      return TonightScreen();
    }

    switch (selectedHomeItemIndex) {
      case 0:
        return WeekScreen();
      case 1:
        return PartyScreen();
      case 2:
        return MusicScreen();
      case 3:
        return CultureScreen();
      case 4:
        return Icon(Icons.event, size: 100);
      case 5:
        return Icon(Icons.event, size: 100);
      case 6:
        return Icon(Icons.event, size: 100);
      case 7:
        return Icon(Icons.event, size: 100);
      case 8:
        return Icon(Icons.event, size: 100);

      case 9:
        return Icon(Icons.event, size: 100);

      case 10:
        return Icon(Icons.event, size: 100);

      default:
        return Center(child: Text("Content for selected item"));
    }
  }
}
