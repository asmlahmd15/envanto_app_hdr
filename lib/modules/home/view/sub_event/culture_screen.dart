import 'package:flutter/material.dart';

class CultureScreen extends StatefulWidget {
  const CultureScreen({Key? key}) : super(key: key);

  @override
  State<CultureScreen> createState() => _CultureScreenState();
}

class _CultureScreenState extends State<CultureScreen> {
  @override
  Widget build(BuildContext context) {
    return Icon(Icons.event, size: 100);
  }
}
