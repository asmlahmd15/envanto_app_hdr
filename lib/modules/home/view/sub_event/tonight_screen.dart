import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/home/view/widget/film_item_home.dart';
import 'package:envanto_app/modules/layout/view/widget/popular_home_item.dart';
import 'package:flutter/material.dart';

class TonightScreen extends StatefulWidget {
  const TonightScreen({Key? key}) : super(key: key);

  @override
  State<TonightScreen> createState() => _TonightScreenState();
}

class _TonightScreenState extends State<TonightScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: ColorApp.black(context),
        child: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 20), // Added for some spacing

                SizedBox(
                  height: 20,
                ),

                FilmItemHome(),
                FilmItemHome(),
                FilmItemHome(),
                PopularHomeItem(),
                SizedBox(
                  height: 30,
                ),
                PopularHomeItem(),
                // PopularHomeItem(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
