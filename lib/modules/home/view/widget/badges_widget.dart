import 'package:envanto_app/global/components/title_body.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;
class BadgesWidget extends StatelessWidget {
  const BadgesWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  badges.Badge(
      showBadge: true,
      ignorePointer: false,
      onTap: () {},
      badgeContent:
      CircleAvatar(
        radius: 6,
        child: TitleBody(text: '5',),
        backgroundColor: ColorApp.fuchsia(context),
      ),
      badgeAnimation: const badges.BadgeAnimation.rotation(
        animationDuration: Duration(seconds: 1),
        colorChangeAnimationDuration: Duration(seconds: 1),
        loopAnimation: false,
        curve: Curves.fastOutSlowIn,
        colorChangeAnimationCurve: Curves.easeInCubic,
      ),
      child: const Icon(Icons.shopping_cart),
    );
  }
}
