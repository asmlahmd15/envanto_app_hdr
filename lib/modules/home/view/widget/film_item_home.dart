import 'package:envanto_app/global/components/text_app_smail.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
 import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/event_details/view/screen/event_details_screen.dart';
 import 'package:flutter/material.dart';

import 'package:video_player/video_player.dart';


class FilmItemHome extends StatefulWidget {
  const FilmItemHome({Key? key}) : super(key: key);

  @override
  State<FilmItemHome> createState() => _FilmItemHomeState();
}

class _FilmItemHomeState extends State<FilmItemHome> {
  late VideoPlayerController _controller;
  bool _initialized = false;
  bool isStart=true;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(Uri.parse(
        'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4'))
      ..initialize().then((_) {
        setState(() {
          _initialized = true;
        });
      });
  }
  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: ColorApp.primaryBackground(context),
        ),
        width: double.infinity,
        height: 300,
        child: InkWell(
          onTap: () {
            if (_controller.value.isPlaying) {
              setState(() {
                isStart=true;
                _controller.pause();
              });

            } else {
              setState(() {
                isStart=false;
                _controller.play();
              });

            }
          },


          splashColor: Colors.transparent,
          focusColor: Colors.transparent,
          hoverColor: Colors.transparent,
          highlightColor: Colors.transparent,



          child: SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: Stack(
              // alignment: AlignmentDirectional.centerStart,
              children: [
                  _initialized ?
                  AspectRatio(
                    aspectRatio: _controller.value.aspectRatio,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: VideoPlayer(_controller),
                    ),
                  ):
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.asset(
                    ImageAssets.screen,
                    width: double.infinity,
                    height: 283,
                    fit: BoxFit.fill,
                  ),
                ),

                Container(
                  width: double.infinity,
                  height: 284,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        const Color(0x0014181B),
                        ColorApp.primaryBackground(context),
                      ],
                      stops: [0, 1],
                      begin: const AlignmentDirectional(0, -1),
                      end: const AlignmentDirectional(0, 1),
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsetsDirectional.symmetric(
                      vertical: 16, horizontal: 16),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 50,
                        height: 24,
                        decoration: BoxDecoration(
                          color: ColorApp.primaryBackground(context),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Align(
                          alignment: const AlignmentDirectional(0, 0),
                          child: Text(
                            '23\$',
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.bodyMedium,
                          ),
                        ),
                      ),
                      Align(
                        alignment: const AlignmentDirectional(0, 0),
                        child: Padding(
                          padding:
                          const EdgeInsetsDirectional.fromSTEB(0, 10, 0, 0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('marshmallow ',
                                  style:
                                  Theme.of(context).textTheme.titleMedium),
                              Container(
                                width: 50,
                                height: 24,
                                decoration: const BoxDecoration(
                                  // color: ColorApp.primaryBackground(context),
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    isStart ?   Icon(
                                      Icons.play_arrow_outlined,
                                      color: ColorApp.white(context),
                                      size: 25,
                                    ) : Icon(
                                      Icons.stop,
                                      color: ColorApp.white(context),
                                      size: 25,
                                    ),


                                    Icon(
                                      Icons.favorite_border,
                                      color: ColorApp.white(context),
                                      size: 20,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const Align(
                          alignment: Alignment.centerLeft,
                          child: TextAppSmail(
                            text: 'Wed 16 Oct',
                          )),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Hello World',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                              color: ColorApp.white(context).withOpacity(0.5),
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              navigateTo(context, EventDetailsScreen());
                            },
                            child: const Align(
                              alignment: AlignmentDirectional(1, 0),
                              child: Icon(
                                Icons.arrow_forward_outlined,
                                // color: FlutterFlowTheme.of(context).secondaryText,
                                size: 24,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



