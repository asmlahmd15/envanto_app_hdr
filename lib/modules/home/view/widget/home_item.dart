import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class HomeItem extends StatelessWidget {
  final String image;
  final String title;
  final bool isSelected;

  const HomeItem(
      {Key? key,
      required this.image,
      required this.title,
      required this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 85,
      height: 65,
      child: Column(
        children: [
          Container(
            width: 55,
            height: 55,
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border.all(
                  color: isSelected
                      ? ColorApp.fuchsia(context)
                      : Colors.transparent),
              color: ColorApp.grey(context).withOpacity(0.15),
              // color: isSelected ? Colors.red : Colors.transparent,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Image.asset(
              image,
              fit: BoxFit.contain,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Text(
              title,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontSize: 12),
            ),
          ),
        ],
      ),
    );
  }
}
