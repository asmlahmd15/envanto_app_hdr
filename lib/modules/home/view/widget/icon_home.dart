import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class IconHome extends StatelessWidget {
  final Function()? onTap;
  final IconData icon;
  const IconHome({Key? key, this.onTap, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Icon(icon,color: ColorApp.white(context),size: 30),
    );
  }
}
