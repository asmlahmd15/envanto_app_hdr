import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchHome extends StatefulWidget {
  const SearchHome({Key? key}) : super(key: key);

  @override
  State<SearchHome> createState() => _SearchHomeState();
}

class _SearchHomeState extends State<SearchHome> {
  @override
  Widget build(BuildContext context) {
    return // Generated code for this TextField Widget...
      Padding(
        padding: EdgeInsetsDirectional.fromSTEB(16, 0, 16, 16),
        child: TextFormField(
          // controller: _model.textController,
          // onChanged: (_) => EasyDebounce.debounce(
          //   '_model.textController',
          //   Duration(milliseconds: 2000),
          //       () => setState(() {}),
          // ),
          autofocus: true,
          obscureText: false,
          decoration: InputDecoration(
            hintText: 'Search event',
            hintStyle: GoogleFonts.getFont(
              'Inter',
              // color: FlutterFlowTheme.of(context).primaryBtnText,
              fontWeight: FontWeight.normal,
              fontSize: 14,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            focusedErrorBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            filled: true,
            // fillColor: FlutterFlowTheme.of(context).lineColor,
            contentPadding: EdgeInsetsDirectional.fromSTEB(0, 20, 0, 20),
            prefixIcon: Icon(
              Icons.search,
              color: Color(0x96ED157A),
              size: 14,
            ),
            // suffixIcon: _model.textController!.text.isNotEmpty
            //     ? InkWell(
            //   onTap: () async {
            //     _model.textController?.clear();
            //     setState(() {});
            //   },
            //   child: Icon(
            //     Icons.clear,
            //     color: Color(0x96ED157A),
            //     size: 18,
            //   ),
            // )
            //     : null,
          ),
          style: GoogleFonts.getFont(
            'Inter',
            color: Colors.white,
            fontWeight: FontWeight.normal,
            fontSize: 14,
          ),
          // validator: _model.textControllerValidator.asValidator(context),
        ),
      );

  }
}
