import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/home/view/screen/home_screen.dart';
import 'package:envanto_app/modules/my_event/view/screen/my_event_screen.dart';
import 'package:envanto_app/modules/reels/view/screen/reels_screen.dart';
import 'package:envanto_app/modules/search/view/screen/search_screen.dart';
import 'package:envanto_app/modules/view_profile/view/screen/view_profile_screen.dart';
import 'package:flutter/material.dart';

class LayoutScreen extends StatefulWidget {
  const LayoutScreen({Key? key}) : super(key: key);

  @override
  State<LayoutScreen> createState() => _LayoutScreenState();
}

class _LayoutScreenState extends State<LayoutScreen> {
  int _currentIndex = 0;

  final List<Widget> _children = [
    // MyEventScreen(),
    // SearchScreen(),
    // HomeScreen(),
    // MyEventScreen(),

    HomeScreen(),
    MyEventScreen(),
    SearchScreen(),
    ReelAppScreen(),
    ViewProfileScreen(),

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: ColorApp.black(context),
      body: _children[_currentIndex],
      bottomNavigationBar: Container(
        color: Colors.purple,
        child: BottomNavigationBar(
          backgroundColor: ColorApp.white(context),
          onTap: onTabTapped,
          selectedItemColor: ColorApp.fuchsia(context),
          unselectedItemColor: Colors.grey,
          currentIndex: _currentIndex,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home_outlined,
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'Messages',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: 'Profile',
            ),

            BottomNavigationBarItem(
              icon: Icon(Icons.video_collection),
              label: 'Reels Evinto',
            ),


            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: 'Profile',
            ),
          ],
        ),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
