import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/home/view/screen/home_screen.dart';
import 'package:envanto_app/modules/layout/view/widget/popular_item_home.dart';
import 'package:flutter/material.dart';

class PopularHomeItem extends StatelessWidget {
  const PopularHomeItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: ColorApp.primaryBackground(context),
        ),
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Align(
                alignment: const AlignmentDirectional(0, -1),
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding:
                        const EdgeInsetsDirectional.fromSTEB(5, 5, 5, 5),
                        child: Text(
                          'Popular',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(5, 5, 5, 5),
                        child: Icon(
                          Icons.arrow_circle_right_sharp,
                          color: ColorApp.white(context).withOpacity(0.5),
                          size: 30,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                children: [
                  const PopularItemHome(),
                  const PopularItemHome(),
                  const PopularItemHome(),
                ],
              ),
            ]),
      ),
    );
  }
}