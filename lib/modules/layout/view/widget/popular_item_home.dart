import 'package:envanto_app/global/components/text_app_smail.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

class PopularItemHome extends StatelessWidget {
  const PopularItemHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Align(
        alignment: const AlignmentDirectional(0, 0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: const AlignmentDirectional(-1, 0),
              child: Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(5, 5, 0, 5),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.asset(
                    ImageAssets.screen,
                    width: 45,
                    height: 45,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: const AlignmentDirectional(-1, 0),
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(10, 0, 0, 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Ann Marie',
                          style: Theme.of(context).textTheme.bodyMedium),
                      const TextAppSmail(
                        text: 'Wed 18 Aug',
                      ),
                      Text(
                        'London',
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const Align(
              alignment: AlignmentDirectional(1, 0),
              child: Padding(
                padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
                child: Icon(
                  Icons.favorite_border,
                  // color: FlutterFlowTheme.of(context).secondaryText,
                  size: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
