import 'package:envanto_app/global/components/appbar_app_event.dart';

import 'package:envanto_app/global/components/button_medium_app.dart';

import 'package:envanto_app/global/components/logo_app.dart';

import 'package:envanto_app/global/components/text_display_large.dart';
import 'package:envanto_app/global/components/text_form_field_app.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/modules/layout/view/screen/layout_screen.dart';
import 'package:envanto_app/modules/signup/view/screen/signup_screen.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  appbarAppEvent(title: 'Login'),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const LogoApp(),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextDisplayLarge(text: 'Welcome Back,'),
              ),
              const SizedBox(height: 26),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  'Fill out the information below in order to access your account.',
                  style: Theme.of(context).textTheme.bodySmall!.copyWith(
                        color: ColorApp.white(context).withOpacity(0.5),
                        fontSize: 14,
                      ),
                ),
              ),
              const SizedBox(height: 26),
              const TextFormFieldApp(
                keyboardType: TextInputType.emailAddress,
                labelText: 'email',
                hintText: 'Enter your email here...',
              ),
              TextFormFieldApp(
                keyboardType: TextInputType.visiblePassword,
                suffixIcon: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18),
                  child: Icon(
                    Icons.visibility_off_outlined,
                    color: ColorApp.white(context),
                  ),
                ),
                labelText: 'password',
                hintText: 'Enter your password here...',
              ),
              const SizedBox(
                height: 20,
              ),
                Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const TextDisplayLarge(text: 'Forgot Password?',sizeFont: 16),


                    ButtonMediumApp(

                      width: 130,
                      height: 50,

                      onTap: (){
                        navigateAndFinish(context, const LayoutScreen());
                      },
                      text: 'Login',fontSize: 18,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Don't have an account?",
                      style: Theme.of(context).textTheme.bodySmall!.copyWith(
                            color: ColorApp.white(context).withOpacity(0.8),
                            fontSize: 14,
                          ),
                    ),
                    InkWell(
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,

                      onTap: (){
                        navigateTo(context,  SignUpScreen());
                      },
                      child: Text(
                        "Create Account",
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              color: ColorApp.secondary,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 40,),
            ],
          ),
        ),
      ),
    );
  }
}
