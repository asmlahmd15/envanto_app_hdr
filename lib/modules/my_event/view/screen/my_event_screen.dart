import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/my_event/view/widget/completed_widget.dart';
import 'package:envanto_app/modules/my_event/view/widget/upcoming_widget.dart';
import 'package:flutter/material.dart';

class MyEventScreen extends StatefulWidget {
  const MyEventScreen({Key? key}) : super(key: key);

  @override
  State<MyEventScreen> createState() => _MyEventScreenState();
}

class _MyEventScreenState extends State<MyEventScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabSelection);
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: ColorApp.black(context),
      appBar: AppBar(
        backgroundColor: ColorApp.black(context),
        elevation: 0.0,
        title: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'My Events',
            style: TextStyle(
              color: ColorApp.white(context),
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      body: SafeArea(
        top: true,
        child: Column(
          children: [
            Align(
              alignment: const Alignment(0, 0),
              child: TabBar(
                labelColor: ColorApp.secondary,
                unselectedLabelColor: ColorApp.ashen(context),
                labelStyle: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                unselectedLabelStyle: const TextStyle(

                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
                indicatorColor: ColorApp.secondary,
                indicatorWeight: 4,
                tabs: const [
                  Tab(text: 'Upcoming'),
                  Tab(text: 'Completed'),
                ],
                controller: _tabController,
              ),
            ),
            if (_tabController.index == 0)
              const Expanded(child: UpcomingWidget()),
            if (_tabController.index == 1)
              const Expanded(child: CompletedWidget()),
          ],
        ),
      ),
    );
  }
}
