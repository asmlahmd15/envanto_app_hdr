import 'package:envanto_app/global/components/button_medium_app.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

class CompletedItem extends StatelessWidget {
  const CompletedItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 2),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        color: ColorApp.primaryBackground(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: Image.asset(
                ImageAssets.partyPhoto,
                width: double.infinity,
                height: 140,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
              child: Text(
                'Busted | London ',
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    color: ColorApp.white(context),
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Thu 10 Aug',
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      color: ColorApp.grey(context),
                      fontSize: 20,
                    ),
                  ),

                  ButtonMediumApp(
                    width: 80,
                    height: 44,

                    fontSize: 12,
                    text: 'Rate Event',
                    onTap: () {},
                  )

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}