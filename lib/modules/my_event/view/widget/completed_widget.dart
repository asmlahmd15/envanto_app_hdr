
import 'package:envanto_app/modules/my_event/view/widget/completed_item.dart';
import 'package:flutter/material.dart';

class CompletedWidget extends StatelessWidget {
  const CompletedWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   ListView.builder(
      itemCount: 100,
      itemBuilder: (context, index) {
        return CompletedItem();
      },
    );
  }
}



