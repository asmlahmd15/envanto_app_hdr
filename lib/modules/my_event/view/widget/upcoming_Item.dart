import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

class UpcomingItem extends StatelessWidget {
  const UpcomingItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:   EdgeInsets.symmetric(horizontal: 16, vertical: 2),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        color: ColorApp.primaryBackground(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: Image.asset(
                ImageAssets.partyPhoto,
                width: double.infinity,
                height: 140,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
              child: Text(
                'Field Day 2023',
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    color: ColorApp.white(context),
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Sat 19 Aug',
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      color: ColorApp.grey(context),
                    ),
                  ),
                  Text(
                    '250 sp',
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      fontSize: 22,
                      color: ColorApp.grey(context),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    height: 0,
                  ),
                  Text(
                    'Total',
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      fontSize: 14,
                      color: ColorApp.grey(context),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}