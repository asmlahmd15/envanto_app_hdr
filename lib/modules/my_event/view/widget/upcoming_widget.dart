
import 'package:envanto_app/modules/my_event/view/widget/upcoming_Item.dart';
import 'package:flutter/material.dart';

class UpcomingWidget extends StatelessWidget {
  const UpcomingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 10,
      itemBuilder: (context, index) {
        return const UpcomingItem();
      },
    );
  }
}


