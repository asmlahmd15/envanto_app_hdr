import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/components/logo_app.dart';
import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/screen/layers_step_screen.dart';
import 'package:flutter/material.dart';

class OnboardScreen extends StatefulWidget {
  const OnboardScreen({Key? key}) : super(key: key);

  @override
  State<OnboardScreen> createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorApp.black(context),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(ImageAssets.onboardImage),
                fit: BoxFit.fill,
              ),
            ),
          ),
            Column(
            children: [
              LogoApp(),
              Spacer(),
              TextAppTitle(text: 'Find your favourite\n MOOD'),
              SizedBox(height: 20,),
              TextAppSub(
                text:
                    '\"Bring your dream events to life effortlessly! With intuitive tools, manage guest lists, venues, schedules, and more, right from the palm of your hand.\"',
              ),

              ButtonApp(
                onTap: (){
                  navigateTo(context,LayersStepScreen());
                },
                // onTap: navigateTo(context,),
                text: 'Get Started',
              ),
            ],
          ),
        ],
      ),
    );
  }
}
