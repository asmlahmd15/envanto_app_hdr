import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/reels/view/widget/account_profile_item.dart';
import 'package:envanto_app/modules/reels/view/widget/icon_reels.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:video_player/video_player.dart';

class ReelAppScreen extends StatefulWidget {
  const ReelAppScreen({super.key});

  @override
  _ReelAppScreenState createState() => _ReelAppScreenState();
}

class _ReelAppScreenState extends State<ReelAppScreen> {
  PageController _pageController = PageController();
  List<String> videoUrls = [
    'https://assets.mixkit.co/videos/preview/mixkit-father-and-his-little-daughter-eating-marshmallows-in-nature-39765-large.mp4',
    'https://assets.mixkit.co/videos/preview/mixkit-father-and-his-little-daughter-eating-marshmallows-in-nature-39765-large.mp4',
    'https://assets.mixkit.co/videos/preview/mixkit-father-and-his-little-daughter-eating-marshmallows-in-nature-39765-large.mp4',
  ];

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView.builder(
        scrollDirection: Axis.vertical,
        controller: _pageController,
        itemCount: videoUrls.length,
        itemBuilder: (context, index) {
          return VideoReelItem(videoUrl: videoUrls[index]);
        },
      ),
    );
  }
}

class VideoReelItem extends StatefulWidget {
  final String videoUrl;

  VideoReelItem({super.key, required this.videoUrl});

  @override
  _VideoReelItemState createState() => _VideoReelItemState();
}

class _VideoReelItemState extends State<VideoReelItem>
    with TickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> heartAnimation;
  late VideoPlayerController _controller;
  bool _initialized = false;

  void _onVideoTap() {
    setState(() {
      _showHeart = true;
    });

    controller.forward();
  }

  bool _showHeart = false;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(Uri.parse(widget.videoUrl))
      ..initialize().then((_) {
        setState(() {
          _initialized = true;
          _controller.play();
        });
      });

    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    heartAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.fastLinearToSlowEaseIn,
        // curve: Curves.fastOutSlowIn,
      ),
    );

    heartAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        setState(() {
          _showHeart = false;
        });
      }
    });
    // reStart
    _controller.addListener(() {
      if (_controller.value.position >= _controller.value.duration) {
        _controller.seekTo(Duration.zero);
        _controller.play();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onDoubleTap: _onVideoTap,
      onTap: () {
        setState(() {
          if (_controller.value.isPlaying) {
            _controller.pause();
          } else {
            _controller.play();
          }
        });
      },
      child: Center(
        child: _controller.value.isInitialized
            ? Stack(
                children: [
                  VideoPlayer(_controller),
                  IconReels(
                    urlVideo: widget.videoUrl,
                  ),
                  AccountProfileItem(),
                  if (_showHeart)
                    Center(
                      child: AnimatedBuilder(
                        animation: heartAnimation,
                        builder: (BuildContext context, Widget? child) {
                          return Transform.scale(
                            scale: heartAnimation.value,
                            child: child,
                          );
                        },
                        child: const Icon(
                          Icons.favorite,
                          color: Colors.red,
                          size: 200.0,
                        ),
                      ),
                    ),
                ],
              )
            : CircularProgressIndicator(
                color: ColorApp.black(context),
                backgroundColor: ColorApp.white(context)),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
