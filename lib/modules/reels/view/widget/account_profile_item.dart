import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

class AccountProfileItem extends StatelessWidget {
  const AccountProfileItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // Profile Image
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30), // Making it a circle
              image: const DecorationImage(
                image: AssetImage(ImageAssets.partyPerson),
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(width: 12.0), // A little space between image and text

          // Text Content
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  'name Account',
                  style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      color: ColorApp.white(context),
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                  maxLines: 2, // Limiting to two lines for a cleaner look
                  overflow: TextOverflow.ellipsis, // ... for overflow text
                ),
                const SizedBox(height: 4.0),
                Text(
                  'Some additional details or username',
                  style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      color: ColorApp.white(context),
                      fontWeight: FontWeight.w400,
                      fontSize: 12),
                ),
                const SizedBox(height: 6.0),
                Text(
                  'a description',
                  style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      color: ColorApp.white(context),
                      fontWeight: FontWeight.w700,
                      fontSize: 12),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
