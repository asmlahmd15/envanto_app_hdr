import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

Widget commentItem({required BuildContext context}) => ListTile(
  leading: Container(
    width: 45,
    height: 45,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(20),
      image: const DecorationImage(
        image: AssetImage(ImageAssets.partyPerson),
        fit: BoxFit.cover,
      ),
    ),
  ),
  title: Text(
    'Comments you write',
    style: Theme.of(context).textTheme.displayMedium!.copyWith(
      fontSize: 16.0,
    ),
  ),
);