import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/reels/view/screen/reels_screen.dart';
import 'package:envanto_app/modules/reels/view/widget/comment_item.dart';
import 'package:flutter/material.dart';

class commentReels extends StatelessWidget {
  const commentReels({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: 5,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: commentItem(context: context),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(
                    color: ColorApp.black(context),
                  ),
                ),
                suffixIcon: Icon(Icons.send),
                hintText: 'Enter a search term',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
