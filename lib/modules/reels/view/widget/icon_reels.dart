import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/modules/reels/view/screen/reels_screen.dart';
import 'package:envanto_app/modules/reels/view/widget/comment_reels.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class IconReels extends StatefulWidget {
  final String urlVideo;

  const IconReels({Key? key, required this.urlVideo}) : super(key: key);

  @override
  State<IconReels> createState() => _IconReelsState();
}

class _IconReelsState extends State<IconReels> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      child: Align(
        alignment: Alignment.bottomRight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.favorite,
                  color: ColorApp.white(context),
                )),
            IconButton(
                onPressed: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return commentReels();
                    },
                  );
                },
                icon: const Icon(Icons.chat)),
            IconButton(
                onPressed: () {
                  Share.share(widget.urlVideo);
                },
                icon: const Icon(Icons.shortcut_sharp)),
          ],
        ),
      ),
    );
  }
}