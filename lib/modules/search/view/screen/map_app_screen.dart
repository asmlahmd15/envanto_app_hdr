import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapAppScreen extends StatefulWidget {
  const MapAppScreen({Key? key}) : super(key: key);

  @override
  State<MapAppScreen> createState() => _MapAppScreenState();
}

class _MapAppScreenState extends State<MapAppScreen> {
  GoogleMapController? _controller;
  LatLng? _currentPosition;
  final Set<Marker> _markers = {};

  final List<LatLng> _imaginaryPoints = [
    const LatLng(37.7749, -122.4194),  // San Francisco, CA
    const LatLng(34.0522, -118.2437),  // Los Angeles, CA
    const LatLng(40.7128, -74.0060),   // New York, NY
    const LatLng(41.8781, -87.6298),   // Chicago, IL
    const LatLng(34.0522, -84.3880),   // Atlanta, GA
  ];

  @override
  void initState() {
    super.initState();
    _determinePosition();
    _addImaginaryMarkers();
  }




  void _addImaginaryMarkers() {
    List<BitmapDescriptor> colors = [
      BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
      BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
      BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow),
      BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
    ];

    for (int i = 0; i < _imaginaryPoints.length; i++) {
      _markers.add(Marker(
        markerId: MarkerId('point_$i'),
        position: _imaginaryPoints[i],
        infoWindow: InfoWindow(title: 'Point ${i + 1}'),
        icon: colors[i],
      ));
    }
  }




  Future<void> _determinePosition() async {
    Location location = Location();
    bool? serviceEnabled;
    PermissionStatus? permissionGranted;

    serviceEnabled = await location.serviceEnabled();
    if (serviceEnabled == false) {
      serviceEnabled = await location.requestService();
      if (serviceEnabled == false) {
        return;
      }
    }

    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    LocationData? locationData;
    try {
      locationData = await location.getLocation();
    } catch (e) {
      // Handle any error here
      return;
    }

    setState(() {
      _currentPosition = LatLng(locationData!.latitude!, locationData.longitude!);
      _markers.add(Marker(
        markerId: const MarkerId("my_location"),
        position: _currentPosition!,
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        markers: _markers,
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: _currentPosition ?? const LatLng(0, 0), // Default to (0,0) if _currentPosition is null
          zoom: 14,
        ),
        onMapCreated: (GoogleMapController controller) {
          _controller = controller;
        },
      ),
    );
  }
}
