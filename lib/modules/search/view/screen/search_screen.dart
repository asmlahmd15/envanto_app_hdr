import 'package:envanto_app/global/components/appbar_app_event.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/modules/search/view/screen/map_app_screen.dart';
import 'package:envanto_app/modules/search/view/widget/action_button_search.dart';
import 'package:envanto_app/modules/search/view/widget/item_previous_search.dart';
import 'package:envanto_app/modules/search/view/widget/item_search.dart';
import 'package:envanto_app/modules/search/view/widget/search_widget.dart';
import 'package:flutter/material.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorApp.black(context),
      floatingActionButton:  ActionButtonSearch(
        onPressed: (){

          navigateTo(context,MapAppScreen());
        },


      ),
      appBar: appbarAppEvent(title: 'Search'),
      body: SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SearchWidget(),
              const Row(
                children: [
                  ItemPreviousSearch(
                    title: 'Date',
                  ),
                  ItemPreviousSearch(
                    title: 'Price',
                  ),
                  ItemPreviousSearch(
                    title: 'Location',
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                child: Text(
                  'Recently Viewed',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
              const ItemSearch(),
              const ItemSearch(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                child: Text(
                  'Popular on Evanto',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
              const ItemSearch(),
              const ItemSearch(),
              const ItemSearch(),
              const ItemSearch(),
            ]),
      ),
    );
  }
}
