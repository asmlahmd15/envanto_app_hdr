import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class ActionButtonSearch extends StatelessWidget {
  final void Function()? onPressed;

  const ActionButtonSearch({Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return // Generated code for this FloatingActionButton Widget...
        FloatingActionButton.extended(
      onPressed: onPressed,
      backgroundColor: ColorApp.fuchsia(context),
      icon: Icon(
        Icons.location_on,
        color: ColorApp.white(context),
        size: 20,
      ),
      elevation: 8,
      label: Text(
        'VIEW MAP',
        style: Theme.of(context).textTheme.bodyMedium,
      ),
    );
  }
}
