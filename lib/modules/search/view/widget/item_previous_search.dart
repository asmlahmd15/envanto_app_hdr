import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class ItemPreviousSearch extends StatefulWidget {
  final String title;

  const ItemPreviousSearch({Key? key, required this.title}) : super(key: key);

  @override
  _ItemPreviousSearchState createState() => _ItemPreviousSearchState();
}

class _ItemPreviousSearchState extends State<ItemPreviousSearch> {
  bool _isPressed = false; // To keep track of the tap state

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.all(5),
      child: InkWell(
        onTap: () {
          setState(() {
            _isPressed = !_isPressed;
          });
        },
        borderRadius: BorderRadius.circular(20),
        child: Container(
          decoration: BoxDecoration(
            color: _isPressed ? ColorApp.fuchsia(context) : ColorApp.primaryBackground(context),
            // Conditional color assignment
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsetsDirectional.symmetric(
                horizontal: 16, vertical: 10),
            child: Text(
              widget.title,
              style: Theme.of(context).textTheme.displaySmall!.copyWith(
                    color: _isPressed
                        ? ColorApp.white(context)
                        : ColorApp.white(context)
                            .withOpacity(0.5), // Conditional text color
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
