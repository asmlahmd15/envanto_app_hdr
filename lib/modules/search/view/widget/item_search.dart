import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

class ItemSearch extends StatelessWidget {
  const ItemSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return // Generated code for this Row Widget...
      Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(20, 10, 10, 10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                ImageAssets.partyPhoto,
                width: 45,
                height: 45,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'The Partisans',
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: ColorApp.white(context).withOpacity(0.8),
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,


                    ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(0, 0, 5, 0),
                    child: Text(
                      'Sat 19 August  ',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(

                        color: ColorApp.white(context).withOpacity(0.8),
                        fontWeight: FontWeight.w500,
                        fontSize: 14.0,
                          ),
                      ),
                    ),
                ],
              ),
            ],
          ),
        ],
      )
    ;
  }
}
