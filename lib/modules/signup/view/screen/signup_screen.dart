import 'package:envanto_app/global/components/appbar_app_event.dart';
import 'package:envanto_app/global/components/button_medium_app.dart';
import 'package:envanto_app/global/components/logo_app.dart';
import 'package:envanto_app/global/components/text_display_large.dart';
import 'package:envanto_app/global/components/text_form_field_app.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/modules/layout/view/screen/layout_screen.dart';
import 'package:envanto_app/modules/login/view/screen/login_screen.dart';
import 'package:envanto_app/modules/signup/view/widget/button_signup.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool _isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar:  appbarAppEvent(title: 'Sign Up'),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              const LogoApp(),
              SizedBox(
                height: 10,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextDisplayLarge(text: 'Get Started Below,'),
              ),
              const SizedBox(height: 26),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  'Fill out the information below in order to access your account.',
                  style: Theme.of(context).textTheme.bodySmall!.copyWith(
                        color: ColorApp.white(context),
                        fontSize: 14,
                      ),
                ),
              ),
              const SizedBox(height: 26),
              const TextFormFieldApp(
                keyboardType: TextInputType.emailAddress,
                labelText: 'email',
                hintText: 'Enter your email here...',
              ),

              TextFormFieldApp(
                obscureText: _isPasswordVisible,
                // keyboardType : TextInputType.visiblePassword,
                suffixIcon: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        _isPasswordVisible = !_isPasswordVisible;
                      });
                    },
                    child: Icon(
                      _isPasswordVisible
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_rounded,
                      color: ColorApp.white(context),
                    ),
                  ),
                ),
                labelText: 'password',
                hintText: 'Enter your password here...',
              ),

              // const SizedBox(
              //   height: 50,
              // ),

              ButtonSignUp(onTap: (){
                navigateAndFinish(context, LayoutScreen());

              },

                text: 'Create Account',
                color: ColorApp.white(context),
                textColor: ColorApp.black(context),
              ),

              Center(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Text(
                    'Or sign in with',
                    style: Theme.of(context).textTheme.bodySmall!.copyWith(
                          color: ColorApp.white(context),
                          fontSize: 14,
                        ),
                  ),
                ),
              ),

              ButtonSignUp(
                text: 'Continue with Google',
                child: Icon(
                  FontAwesomeIcons.google,
                  color: ColorApp.white(context),
                ),
              ),

              ButtonSignUp(
                text: 'Continue with Facebook',
                child: Icon(
                  FontAwesomeIcons.facebook,
                  color: ColorApp.white(context),
                ),
              ),

              SizedBox(
                height: 10,
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "Already have an account?",
                      style: Theme.of(context).textTheme.bodySmall!.copyWith(
                            color: ColorApp.white(context).withOpacity(0.8),
                            fontSize: 16,
                          ),
                    ),
                    InkWell(
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        navigateAndFinish(context, LoginScreen());
                      },
                      child: Text(
                        "Login",
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              color: ColorApp.secondary,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 30,)
            ],
          ),
        ),
      ),
    );
  }
}
