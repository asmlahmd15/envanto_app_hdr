import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class ButtonSignUp extends StatelessWidget {

  final String text;
  final Color? color;
  final Color? textColor;
  final VoidCallback? onPressed;
  final Widget? child;
  final void Function()? onTap;

  const ButtonSignUp({Key? key,   required this.text,
    this.color,
    this.textColor,
    this.onPressed,
    this.child, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: onTap,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Container(
            width: MediaQuery.of(context).size.width*0.7,
            padding: EdgeInsets.symmetric(vertical: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: color ?? ColorApp.leadBlack(context),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                child?? Container(),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  text,
                  style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    color: textColor ?? ColorApp.white(context).withOpacity(0.8),
                    fontWeight: FontWeight.bold,

                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}