import 'package:day_night_switch/day_night_switch.dart';
import 'package:envanto_app/global/themes/theme_notifier.dart';
import 'package:envanto_app/global/themes/themes.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SwitchThemeApp extends StatefulWidget {
  const SwitchThemeApp({Key? key}) : super(key: key);

  @override
  State<SwitchThemeApp> createState() => _SwitchThemeAppState();
}

class _SwitchThemeAppState extends State<SwitchThemeApp> {
  bool val = false;
  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context, listen: false);

    return  Transform.scale(scale: 0.5,
        child: DayNightSwitch(
          value: val,
          moonImage: AssetImage(ImageAssets.rectangle),
          sunImage: AssetImage(ImageAssets.rectangle2),
          sunColor: Colors.purple,
          moonColor: Colors.blue,
          dayColor: Colors.green,
          nightColor: Colors.red,
          onChanged: (value) {
            setState(() {
              val = value;
              if (val) {
                themeNotifier.setTheme(Themes.customDarkTheme);
              } else {
                themeNotifier.setTheme(Themes.customLightTheme);
              }
            });
          },
        )
    );
  }
}
