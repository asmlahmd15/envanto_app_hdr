import 'package:envanto_app/global/components/button_app.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/modules/signup/view/screen/signup_screen.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/appbar_layers_step.dart';
import 'package:envanto_app/modules/step/screen/step_eight_screen.dart';
import 'package:envanto_app/modules/step/screen/step_five_screen.dart';
import 'package:envanto_app/modules/step/screen/step_four.dart';
import 'package:envanto_app/modules/step/screen/step_nine_screen.dart';
import 'package:envanto_app/modules/step/screen/step_one_screen.dart';
import 'package:envanto_app/modules/step/screen/step_seven_screen.dart';
import 'package:envanto_app/modules/step/screen/step_six_screen.dart';
import 'package:envanto_app/modules/step/screen/step_three_screen.dart';
import 'package:envanto_app/modules/step/screen/step_tow.dart';
import 'package:flutter/material.dart';

class LayersStepScreen extends StatefulWidget {
  const LayersStepScreen({
    Key? key,
  }) : super(key: key);

  @override
  _LayersStepScreenState createState() => _LayersStepScreenState();
}

class _LayersStepScreenState extends State<LayersStepScreen> {
  final PageController _pageController = PageController(initialPage: 0);
  double percent=0.1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorApp.black(context),
        appBar: appBarLayersStep(context: context, percent: percent),

        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: PageView(
                  onPageChanged: ((int pageIndex){

                    setState(() {
                      if (pageIndex == 8) {
                        percent = 1.0;
                      } else {
                        percent = (pageIndex + 1) / 9.0;
                      }
                    });
                  }),
                  controller: _pageController,
                  children: const [
                    StepOneScreen(),
                    StepTowScreen(),
                    StepThreeScreen(),
                    StepFourScreen(),
                    StepFiveScreen(),
                    StepSixScreen(),
                    StepSevenScreen(),
                    StepEightScreen(),
                    StepNineScreen(),
                  ],
                ),
              ),

              ButtonApp(
                text: 'Next',

                  onTap: () {
                    if (_pageController.page!.toInt() == 8) {
                      navigateAndFinish(context,SignUpScreen());

                    } else {
                      _pageController.nextPage(
                        duration: const Duration(milliseconds: 500),
                        curve: Curves.ease,
                      );
                      setState(() {
                        percent+=0.1;
                      });
                    }
                  }
              ),
            ],
          ),
        ));
  }
}



