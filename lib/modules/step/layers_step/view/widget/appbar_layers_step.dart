import 'package:envanto_app/global/components/text_app_smail.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

AppBar appBarLayersStep({required BuildContext context,required double percent}) => AppBar(
      centerTitle: true,
      title: SizedBox(
        width: 200,
        child: LinearPercentIndicator(
          barRadius: const Radius.circular(10),
          animation: true,
          lineHeight: 10.0,
          animationDuration: 1000,
          percent: percent,
          linearStrokeCap: LinearStrokeCap.roundAll,
          fillColor: Colors.transparent,
          progressColor: ColorApp.fuchsia(context),
          backgroundColor: ColorApp.white(context),
        ),
      ),
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      actions: const [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 6),
          child: Center(
            child: TextAppSmail(
              text: 'Skip',
            ),
          ),
        ),
      ],
    );
