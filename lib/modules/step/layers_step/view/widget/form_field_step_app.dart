import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class FromFieldStepApp extends StatelessWidget {
  final Widget? icon;
  final String hintText;

  const FromFieldStepApp(
      {Key? key,
      required this.hintText,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.symmetric(horizontal: 48,vertical: 10),
      child: Container(
          width: double.infinity,
          height: 60,
          decoration: BoxDecoration(
            color: ColorApp.revealBlack(context),
            boxShadow: const [
              BoxShadow(
                blurRadius: 12,
                color: Color(0x0D000000),
                offset: Offset(0, 0),
              )
            ],
            borderRadius: BorderRadius.circular(12),
          ),
          alignment: const AlignmentDirectional(0, 0),
          child: TextFormField(
            // controller: _model.textController,

            obscureText: false,
            decoration: InputDecoration(
              suffixIcon: icon ?? SizedBox(height: 1),
              hintText: hintText,
              hintStyle: Theme.of(context).textTheme.bodySmall,
              enabledBorder: const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0x00000000),
                  width: 1,
                ),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4.0),
                  topRight: Radius.circular(4.0),
                ),
              ),
              focusedBorder: const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0x00000000),
                  width: 1,
                ),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4.0),
                  topRight: Radius.circular(4.0),
                ),
              ),
              errorBorder: const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0x00000000),
                  width: 1,
                ),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4.0),
                  topRight: Radius.circular(4.0),
                ),
              ),
              focusedErrorBorder: const UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0x00000000),
                  width: 1,
                ),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4.0),
                  topRight: Radius.circular(4.0),
                ),
              ),
              contentPadding:
                  const EdgeInsetsDirectional.fromSTEB(24, 0, 24, 0),
            ),
            style: Theme.of(context).textTheme.bodyMedium,
            keyboardType: TextInputType.number,
            // validator: _model.textControllerValidator.asValidator(context),
          )),
    );
  }
}
