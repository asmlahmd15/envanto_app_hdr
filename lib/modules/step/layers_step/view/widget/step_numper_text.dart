import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StepNumberText extends StatelessWidget {
  final String textNumber;
  const StepNumberText({Key? key, required this.textNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: 'STEP ',
              style: GoogleFonts.rubik(

                color:  ColorApp.mauve(context),
                fontWeight: FontWeight.w500,
                fontSize: 14,
              ),
            ),
             TextSpan(
              text: textNumber,
              style: TextStyle(),
            ),
            const TextSpan(
              text: '/',
              style: TextStyle(),
            ),
            const TextSpan(
              text: '9',
              style: TextStyle(),
            )
          ],
          style: GoogleFonts.readexPro(

            color:  ColorApp.mauve(context),
            letterSpacing: 1,
            fontWeight: FontWeight.w500,
          )
        ),
      );

  }
}
