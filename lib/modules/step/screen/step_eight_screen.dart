import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step_url.dart';
import 'package:envanto_app/modules/step/widget/eight_item_sports.dart';
import 'package:flutter/material.dart';

class StepEightScreen extends StatefulWidget {
  const StepEightScreen({Key? key}) : super(key: key);

  @override
  State<StepEightScreen> createState() => _StepEightScreenState();
}

class _StepEightScreenState extends State<StepEightScreen> {
  List<bool> selectedItems = List.generate(9, (index) => false);


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          const StepNumberText(
            textNumber: '8',
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          const TextAppTitle(text: 'Time to customize your interest'),
          SizedBox(
            height: 500,
            child: GridView.count(
              crossAxisCount: 3,
              children: List.generate(9, (index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      selectedItems[index] = !selectedItems[index];
                    });
                  },
                  child: eightItem(
                      image: imageStepUrl[index],
                      isSelected: selectedItems[index], context: context),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}
