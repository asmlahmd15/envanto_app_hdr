import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:envanto_app/modules/step/widget/notifications_feature_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StepFiveScreen extends StatefulWidget {
  const StepFiveScreen({Key? key}) : super(key: key);

  @override
  State<StepFiveScreen> createState() => _StepFiveScreenState();
}

class _StepFiveScreenState extends State<StepFiveScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          const StepNumberText(
            textNumber: '5',
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          const TextAppTitle(text: 'Do you want to turn on notification?'),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          const ImageStep(
            pathUrl: ImageAssets.step5,
          ),

          SizedBox(height: MediaQuery.of(context).size.height * 0.05),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [

            NotificationsFeatureWidget(
              text: 'New weekly healthy reminder',
              iconData: Icons.notifications,
            ),
            NotificationsFeatureWidget(
              text: 'Motivational reminder',
              iconData: Icons.spa,
            ),
            NotificationsFeatureWidget(
              text: 'Personalised program',
              iconData: Icons.personal_video_outlined,
            ),

          ],)


        ],
      ),
    );
  }
}
