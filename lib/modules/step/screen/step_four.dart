import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:flutter/material.dart';

class StepFourScreen extends StatefulWidget {
  const StepFourScreen({Key? key}) : super(key: key);

  @override
  State<StepFourScreen> createState() => _StepFourScreenState();
}

class _StepFourScreenState extends State<StepFourScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(children: [

        SizedBox(height: MediaQuery.of(context).size.height*0.1),
        const StepNumberText(textNumber: '4',),
        SizedBox(height: MediaQuery.of(context).size.height*0.02),
        const TextAppTitle(text: 'Enable Fingerprint'),
        SizedBox(height: MediaQuery.of(context).size.height*0.02),
        const TextAppSub(text: 'If you enable touch ID, you don’t need to enter password when you login'),
        const ImageStep(pathUrl: ImageAssets.step4),

      ],


      ),
    );
  }
}
