import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/drop_down_food.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:flutter/material.dart';

class StepNineScreen extends StatefulWidget {
  const StepNineScreen({Key? key}) : super(key: key);

  @override
  State<StepNineScreen> createState() => _StepNineScreenState();
}

class _StepNineScreenState extends State<StepNineScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          const StepNumberText(
            textNumber: '9',
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          const TextAppTitle(text: 'Enter your location'),
          const TextAppSub(text: 'Please enter where do you live'),
          const ImageStep(
            pathUrl: ImageAssets.step9,
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          DropdownButtonFood(),
        ],
      ),
    );
  }
}
