import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/form_field_step_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:flutter/cupertino.dart';

class StepOneScreen extends StatefulWidget {
  const StepOneScreen({Key? key}) : super(key: key);

  @override
  State<StepOneScreen> createState() => _StepOneScreenState();
}

class _StepOneScreenState extends State<StepOneScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height*0.1),
          StepNumberText(textNumber: '1',),
          SizedBox(height: MediaQuery.of(context).size.height*0.02),
          TextAppTitle(text: 'Enter your number'),
          SizedBox(height: MediaQuery.of(context).size.height*0.02),

          TextAppSub(text: 'Please enter a valid number to continue'),
          ImageStep(pathUrl: ImageAssets.step1,),

          SizedBox(height: 40,),
          FromFieldStepApp(hintText: 'enter you mobile',),





        ],
      ),
    );
  }
}
