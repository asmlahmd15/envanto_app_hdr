import 'package:carousel_slider/carousel_slider.dart';
import 'package:envanto_app/global/components/text_app_smail.dart';
import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/carousel_slider_widget.dart';
import 'package:flutter/material.dart';

class StepSevenScreen extends StatefulWidget {
  const StepSevenScreen({Key? key}) : super(key: key);

  @override
  State<StepSevenScreen> createState() => _StepSevenScreenState();
}

class _StepSevenScreenState extends State<StepSevenScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          const StepNumberText(
            textNumber: '7',
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          const TextAppTitle(text: 'Profile Picture'),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          const TextAppSub(
              text:
                  'You can select photo from one of this emoji or add your own photo as profile picture'),
          CarouselSliderWidget(),
          SizedBox(height: MediaQuery.of(context).size.height * 0.07),
          const TextAppSub(
              text:
                  'You can select photo from one of this emoji or add your own photo as profile picture'),
          const TextAppSmail(
            text: 'Add Custom Photo',
          )
        ],
      ),
    );
  }
}
