import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:envanto_app/modules/step/widget/item_select_gender.dart';
import 'package:flutter/material.dart';

class StepSixScreen extends StatefulWidget {
  const StepSixScreen({Key? key}) : super(key: key);

  @override
  State<StepSixScreen> createState() => _StepSixScreenState();
}

class _StepSixScreenState extends State<StepSixScreen> {
  String? selectedGender;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          const StepNumberText(
            textNumber: '6',
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          const TextAppTitle(text: 'Which one are you?'),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: ['Male', 'Female']
                    .map(
                      (gender) => itemSelectGender(
                          text: gender,
                          color: gender == 'Male' ? ColorApp.mauve(context) : Colors.white,
                          mainAxisAlignment: gender == 'Male'
                              ? MainAxisAlignment.end
                              : MainAxisAlignment.start,
                          context: context,
                          colorText:
                              gender == 'Male' ? Colors.white : ColorApp.mauve(context),
                          selectedGender: selectedGender,
                          onSelected: (String gender) {
                            setState(() {
                              selectedGender = gender;
                            });
                          }),
                    )
                    .toList(),
              ),
              const Align(
                alignment: AlignmentDirectional.bottomCenter,
                child: ImageStep(
                  pathUrl: ImageAssets.step6,
                ),
              ),
            ],
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.07),
          const TextAppSub(
              text:
                  'To give you a customize experience we need to know your gender'),
        ],
      ),
    );
  }
}
