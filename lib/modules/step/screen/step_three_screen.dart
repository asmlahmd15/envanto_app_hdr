import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/form_field_step_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';





class StepThreeScreen extends StatefulWidget {
  const StepThreeScreen({Key? key}) : super(key: key);

  @override
  State<StepThreeScreen> createState() => _StepThreeScreenState();
}

class _StepThreeScreenState extends State<StepThreeScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height*0.1),
          StepNumberText(textNumber: '3',),
          SizedBox(height: MediaQuery.of(context).size.height*0.02),
          TextAppTitle(text: 'Set your password'),

          SizedBox(height: MediaQuery.of(context).size.height*0.02),

          TextAppSub(text: 'Enter a strong password for your account'),
          ImageStep(pathUrl: ImageAssets.step3,),
          SizedBox(height: MediaQuery.of(context).size.height*0.02),

          FromFieldStepApp(hintText: 'Enter password',
            icon: Icon(Icons.visibility_outlined),),





        ],
      ),
    );
  }
}
