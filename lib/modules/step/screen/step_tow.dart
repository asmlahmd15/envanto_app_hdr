import 'package:envanto_app/global/components/text_app_sub.dart';
import 'package:envanto_app/global/components/text_app_title.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/step/layers_step/view/widget/step_numper_text.dart';
import 'package:envanto_app/modules/step/widget/image_step.dart';
import 'package:envanto_app/modules/step/widget/pin_code_app.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class StepTowScreen extends StatefulWidget {
  const StepTowScreen({Key? key}) : super(key: key);

  @override
  State<StepTowScreen> createState() => _StepTowScreenState();
}

class _StepTowScreenState extends State<StepTowScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [

          SizedBox(height: MediaQuery.of(context).size.height*0.1),
          StepNumberText(textNumber: '2',),
          SizedBox(height: MediaQuery.of(context).size.height*0.02),
          TextAppTitle(text: 'Verify your number'),
          SizedBox(height: MediaQuery.of(context).size.height*0.02),
          TextAppSub(text: 'We’ll text you on 9932797131 to verify your number with a code'),
          ImageStep(pathUrl: ImageAssets.step2,),

          PinCodeApp(),
          SizedBox(height: MediaQuery.of(context).size.height*0.02),
          TextAppTitle(text: 'Send me a new code'),
        ],
      ),
    );

  }
}
