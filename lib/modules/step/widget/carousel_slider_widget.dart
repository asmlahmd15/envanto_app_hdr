import 'package:carousel_slider/carousel_slider.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:flutter/material.dart';

class CarouselSliderWidget extends StatelessWidget {
  final List<String> imgList = [
    ImageAssets.rectangle,
    ImageAssets.rectangle1,
    ImageAssets.rectangle2,
  ];

  CarouselSliderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: CarouselSlider(
        options: CarouselOptions(
          height: 200.0,
          enlargeCenterPage: true,
          // autoPlay: true,
          animateToClosest: true,
          aspectRatio: 16.0,
          autoPlayCurve: Curves.fastOutSlowIn,
          enableInfiniteScroll: true,
          autoPlayAnimationDuration: const Duration(milliseconds: 800),
          viewportFraction: 0.39,
        ),
        items: imgList
            .map((item) =>
                Center(child: Image.asset(item, fit: BoxFit.cover, width: 70)))
            .toList(),
      ),
    );
  }
}
