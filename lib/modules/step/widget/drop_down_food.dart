import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

class DropdownButtonFood extends StatefulWidget {
  const DropdownButtonFood({Key? key}) : super(key: key);

  @override
  State<DropdownButtonFood> createState() => _DropdownButtonFoodState();
}

class _DropdownButtonFoodState extends State<DropdownButtonFood> {
  String dropdownValue = 'State';
  @override
  Widget build(BuildContext context) {
    return    Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 20, 12),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Theme.of(context).primaryColor.withOpacity(0.1),

          border: Border.all(color: ColorApp.primaryBackground(context),width: 2),
        ),
        child: DropdownButton<String>(
          value: dropdownValue,
          icon:   const Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Icon(Icons.keyboard_arrow_down_rounded),
          ),
          iconSize: 15,
          elevation: 2,
          style: Theme.of(context).textTheme.bodyText1,
          underline: Container(

              color: Theme.of(context).primaryColor),
          onChanged: (String? newValue) {
            setState(() {
              dropdownValue = newValue!;
            });
          },
          items: <String>[
            'State',
            'Alabama',
            'Alaska',
            'Arizona',
            'Arkansas',
            'California',
            'Colorado',
            'Connecticut',
            'Delaware',
            'Florida',
            'Georgia',
            'Hawaii',
            'Idaho',
            'Illinois',
            'Indiana',
            'Iowa',
            'Kansas',
            'Kentucky',
            'Louisiana',
            'Maine',
            'Maryland',
            'Massachusetts',
            'Michigan',
            'Minnesota',
            'Mississippi',
            'Missouri',
            'Montana',
            'Nebraska',
            'Nevada',
            'New Hampshire',
            'New Jersey',
            'New Mexico',
            'New York',
            'North Carolina',
            'North Dakota',
            'Ohio',
            'Oklahoma',
            'Oregon',
            'Pennsylvania',
            'Rhode Island',
            'South Carolina',
            'South Dakota',
            'Tennessee',
            'Texas',
            'Utah',
            'Vermont',
            'Virginia',
            'Washington',
            'West Virginia',
            'Wisconsin',
            'Wyoming'
          ].map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(

              value: value,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Text(value),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
