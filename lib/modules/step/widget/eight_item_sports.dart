import 'package:flutter/material.dart';
import 'package:envanto_app/global/utils/color_app.dart';

Widget eightItem({required String image, required bool isSelected,required BuildContext context}) => Padding(
  padding: const EdgeInsets.all(20.0),
  child: Container(
    width: 84,
    height: 74,
    decoration: BoxDecoration(
      color: ColorApp.primaryBackground(context),
      border: isSelected ? Border.all(color: ColorApp.fuchsia(context), width: 2) : null,
      boxShadow: const [
        BoxShadow(
          blurRadius: 24,
          color: Color(0x0D000000),
          offset: Offset(0, 0),
        )
      ],
      borderRadius: BorderRadius.circular(16),
    ),
    child: Padding(
      padding: const EdgeInsetsDirectional.symmetric(
          horizontal: 30, vertical: 30),
      child: Image.asset(
        image,
        width: 50,
        height: 50,
        fit: BoxFit.contain,
      ),
    ),
  ),
);
