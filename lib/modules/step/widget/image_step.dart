import 'package:flutter/cupertino.dart';

class ImageStep extends StatelessWidget {
  final String pathUrl;
  const ImageStep({Key? key, required this.pathUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      pathUrl,
      height: 180,
      fit: BoxFit.contain,
    );

  }
}
