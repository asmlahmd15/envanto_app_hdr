import 'package:envanto_app/global/utils/image_app.dart';

var  imageStepUrl = [
  ImageAssets.sports1,
  ImageAssets.sports2,
  ImageAssets.sports3,
  ImageAssets.sports4,
  ImageAssets.sports5,
  ImageAssets.sports6,
  ImageAssets.sports7,
  ImageAssets.sports8,
  ImageAssets.sports9,
];



var imageHomeUrl = [
  ImageAssets.book,
  ImageAssets.bookT,
  ImageAssets.calendar,
  ImageAssets.discoBall,
  ImageAssets.event,
  ImageAssets.gameController,
  ImageAssets.moon,
  ImageAssets.music,
  ImageAssets.musicT,
  ImageAssets.screen,
];

var imageNameHome = [
  'Tonight',
  'This week',
  'Party',
  'Music',
  'Culture',
  'Title',
  'Title',
  'Title',
  'Title',
  'Title',

];
