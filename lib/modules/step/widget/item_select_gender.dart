import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';

Widget itemSelectGender(
    {required String text,
      required Color color,
      required Color colorText,
      required BuildContext context,
      required MainAxisAlignment mainAxisAlignment,
      required String? selectedGender,
      required Function(String) onSelected}) {
  return GestureDetector(
    onTap: () {
      onSelected(text);
    },
    child: Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(14, 0, 14, 0),
      child: Container(
        width: 156,
        height: 216,
        decoration: BoxDecoration(
          color: color,
          boxShadow: const [
            BoxShadow(
              blurRadius: 24,
              color: Color(0x0D000000),
              offset: Offset(0, 0),
            ),
          ],
          borderRadius: BorderRadius.circular(24),
          border: selectedGender == text
              ? Border.all(color: ColorApp.fuchsia(context), width: 2)
              : null,
        ),
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(16, 16, 16, 16),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: mainAxisAlignment,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                text,
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: colorText,
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
