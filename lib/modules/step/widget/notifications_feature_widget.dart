import 'package:flutter/material.dart';

class NotificationsFeatureWidget extends StatelessWidget {

  final String text;
final IconData iconData;

  const NotificationsFeatureWidget({Key? key, required this.text, required this.iconData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 6),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: 24,
              height: 24,
              decoration: const BoxDecoration(
                color: Color(0xFFED157A),
                shape: BoxShape.circle,
              ),
              alignment: const AlignmentDirectional(0, 0),
              child: Icon(
                iconData,
                color: Colors.white,
                size: 10,
              ),
            ),
             Padding(
              padding: EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
              child: Text(
                text,
                // style: FlutterFlowTheme.of(context).bodyMedium.override(
                //   fontFamily: 'Readex Pro',
                //   fontWeight: FontWeight.normal,
                // ),
              ),
            ),
          ],
        ),
      );

  }
}
