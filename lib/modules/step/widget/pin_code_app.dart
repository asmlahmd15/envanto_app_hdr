import 'package:envanto_app/global/utils/color_app.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinCodeApp extends StatelessWidget {
  const PinCodeApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(48, 48, 48, 0),
      child: PinCodeTextField(
        autoDisposeControllers: false,
        appContext: context,
        length: 4,
        textStyle: Theme.of(context).textTheme.titleSmall,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        enableActiveFill: false,
        // autoFocus: true,
        enablePinAutofill: true,
        errorTextSpace: 16,
        showCursor: true,
        cursorColor: ColorApp.mauve(context),
        obscureText: false,
        hintCharacter: '●',
        pinTheme: PinTheme(
          fieldHeight: 60,
          fieldWidth: 60,
          borderWidth: 2,
          borderRadius: BorderRadius.circular(12),
          shape: PinCodeFieldShape.underline,
          activeColor: ColorApp.black(context),
          inactiveColor: ColorApp.primaryBackground(context),
          selectedColor: ColorApp.revealSilver(context),
          activeFillColor: ColorApp.black(context),
          inactiveFillColor: ColorApp.primaryBackground(context),
          selectedFillColor: ColorApp.secondSilverDetector(context),
        ),
        // controller: _model.pinCodeController,
        onChanged: (_) {},
        autovalidateMode: AutovalidateMode.onUserInteraction,
        // validator: _model.pinCodeControllerValidator.asValidator(context),
      ),
    )
    ;
  }
}
