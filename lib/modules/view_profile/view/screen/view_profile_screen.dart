import 'package:envanto_app/global/components/button_smail_app.dart';
import 'package:envanto_app/global/components/text_app_smail.dart';

import 'package:envanto_app/global/components/text_display_large.dart';
import 'package:envanto_app/global/utils/color_app.dart';
import 'package:envanto_app/global/utils/fun_app.dart';
import 'package:envanto_app/global/utils/image_app.dart';
import 'package:envanto_app/modules/change%20password/view/screen/change_password_screen.dart';
import 'package:envanto_app/modules/edite_profile/view/screen/edite_profile_screen.dart';
import 'package:envanto_app/modules/my_event/view/screen/my_event_screen.dart';
import 'package:envanto_app/modules/signup/view/widget/switch_theam_app.dart';

import 'package:flutter/material.dart';

class ViewProfileScreen extends StatefulWidget {
  const ViewProfileScreen({Key? key}) : super(key: key);

  @override
  State<ViewProfileScreen> createState() => _ViewProfileScreenState();
}

class _ViewProfileScreenState extends State<ViewProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 70,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    border: Border.all(color: ColorApp.secondary, width: 3),
                    image: const DecorationImage(
                      image: AssetImage(ImageAssets.partyPhoto),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextDisplayLarge(
                      text: 'Ahmed Al-Saleh',
                    ),
                    TextAppSmail(
                      text: 'asml9@gmail.com',
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Switch to Light Mode',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(fontWeight: FontWeight.bold, fontSize: 14),
                ),
                const SwitchThemeApp(),
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            ViewProfileItem(
                text: 'Edit Profile',
                onTap: () {
                  navigateTo(context, const EditeProfileScreen());
                }),
            ViewProfileItem(text: 'Change Password', onTap: () {}),
            ViewProfileItem(
                text: 'My Events',
                onTap: () {
                  navigateTo(context, const MyEventScreen());
                }),
            ViewProfileItem(
                text: 'My Bookings',
                onTap: () {
                  navigateTo(context, const ChangePasswordScreen());
                }),
            const SizedBox(height: 70),
            const ButtonSmailApp(
              size: 16,
              height: 50,
              width: 110,
              text: 'Log Out',
            ),
          ],
        ),
      ),
    );
  }
}

class ViewProfileItem extends StatelessWidget {
  final String text;

  final Function()? onTap;

  const ViewProfileItem({Key? key, required this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      child: InkWell(
        splashColor: Colors.transparent,
        focusColor: Colors.transparent,
        hoverColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: onTap,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 55,
          decoration: BoxDecoration(
            // border: Border(
            //   bottom: BorderSide(color: ColorApp.white.withOpacity(0.5), width: 1.0),
            // ),

            borderRadius: BorderRadius.circular(50),

            color: ColorApp.black(context),
            boxShadow: [
              BoxShadow(
                blurRadius: 0,
                color: ColorApp.leadBlack(context),
                offset: const Offset(0, 2),
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsetsDirectional.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  text,
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: ColorApp.white(context),
                      ),
                ),
                const Icon(
                  Icons.chevron_right_rounded,
                  color: Color(0xFF95A1AC),
                  size: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
